<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;
use App\Models\Customer;

class CusmoterTableSeeder extends Seeder
{
    public function run()
    {
        $customer            = new Customer;
        $customer->name      = "Member";
        $customer->telephone = "0918556465";
        $customer->gender    = "28";
        $customer->address   = "82/34 asdff";
        $customer->save();

        $user = new User;
        $user->customer_id  = $customer->id;
        $user->name         = "Member";
        $user->email        = "member@gmail.com";
        $user->username     = "member";
        $user->password     = bcrypt("123456");
        $user->image        = "user.png";
        $user->save();
        $user->roles()->attach(Role::where('name', "User")->first());
    }
}
