<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->customer_id = 0;
        $user->name     = 'Tinnakorn';
        $user->email    = 'tinnakorn@gmail.com';
        $user->username = 'admin';
        $user->password = bcrypt('123456');
        $user->image    = 'user.png';
        $user->save();
        $user->roles()->attach(Role::where('name', "Admin")->first());
    }
}
