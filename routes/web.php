<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { 
    return view('index')
    ->withDriver(App\Models\Driver::all())
    ->withPost(App\Models\Post::orderBy('created_at', 'DESC')->get());
})->name('/');

Route::get('/news/{id}', function ($id) { 
    return view('news')
    ->withPost(App\Models\Post::find($id));
})->name('news');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'CustomerController@store');

Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['middleware' => ['roles'], 'roles' => ['User', 'Admin']], function () {
        Route::get('/customer/{customer}', 'CustomerController@show')->name('customer.show');
        Route::get('/profile/customer/{customer}/edit', 'CustomerController@edit_profile')->name('customer.edit_profile');
        Route::patch('/update_profile/customer/{customer}', 'CustomerController@update_profile')->name('customer.update_profile');
        // Reservation
        Route::resource('/reservation',  'ReservationController'); 
        // Route::post('/reservation/type', 'ReservationController@type')->name('reservation.type');
        Route::post('/reservation/payment', 'ReservationController@payment')->name('reservation.payment');
        Route::get('/reservation/status/{status}',  'ReservationController@status')->name('reservation.status'); 
        Route::post('/reservation/apporve/{apporve}',  'ReservationController@apporve')->name('reservation.apporve'); 
        Route::get('van/detail/{id}', 'VanController@detail')->name('van.detail');
    });

    Route::group(['middleware' => ['roles'], 'roles' => ['Admin']], function () {
        // User
        Route::resource('/user', 'UserController');
        Route::get('/profile/user/{user}/edit', 'UserController@edit_profile')->name('user.edit_profile');
        Route::patch('/update_profile/user/{user}', 'UserController@update_profile')->name('user.update_profile');
        // Customer
        Route::get('/customer', 'CustomerController@index')->name('customer.index');
        Route::delete('/customer/{customer}', 'CustomerController@destroy')->name('customer.destroy');
        Route::get('/customer/{customer}/edit', 'CustomerController@edit')->name('customer.edit');
        Route::patch('/customer/{customer}', 'CustomerController@update')->name('customer.update');
        // Driver
        Route::resource('/driver', 'DriverController');
        // Van
        Route::resource('van', 'VanController');
        // Post
        Route::resource('post', 'PostController');
    });
});
