<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('หน้าหลัก', route('home'));
});

// User
Breadcrumbs::register('user.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ผู้ใช้งานระบบ', route('user.index'));
});
Breadcrumbs::register('user.create', function ($breadcrumbs) {
    $breadcrumbs->parent('user.index');
    $breadcrumbs->push('เพิ่มผู้ใช้งานระบบ', route('user.create'));
});
Breadcrumbs::register('user.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('user.index');
    $breadcrumbs->push('แก้ไขผู้ใช้งานระบบ', route('user.edit', $id));
});
Breadcrumbs::register('user.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ข้อมูลผู้ใช้งานระบบ', route('user.show', $id));
});
Breadcrumbs::register('user.edit_profile', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('user.show', $id);
    $breadcrumbs->push('แก้ไขผู้ใช้งานระบบ', route('user.edit_profile', $id));
});
// Customer 
Breadcrumbs::register('customer.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ข้อมูลลูกค้า', route('customer.index'));
});
Breadcrumbs::register('customer.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('customer.index');
    $breadcrumbs->push('แก้ไขข้อมูลลูกค้า', route('customer.edit', $id));
});
Breadcrumbs::register('customer.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('customer.index');
    $breadcrumbs->push('รายละเอียดลูกค้า', route('customer.show', $id));
});
Breadcrumbs::register('customer.edit_profile', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('customer.show', $id);
    $breadcrumbs->push('แก้ไขข้อมูล', route('customer.edit_profile', $id));
});
// Driver
Breadcrumbs::register('driver.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ข้อมูลคนขับ', route('driver.index'));
});
Breadcrumbs::register('driver.create', function ($breadcrumbs) {
    $breadcrumbs->parent('driver.index');
    $breadcrumbs->push('เพิ่มข้อมูลคนขับ', route('driver.create'));
});
Breadcrumbs::register('driver.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('driver.index');
    $breadcrumbs->push('แก้ไขข้อมูลคนขับ', route('user.edit', $id));
});
Breadcrumbs::register('driver.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('driver.index');
    $breadcrumbs->push('รายละเอียดคนขับ', route('driver.show', $id));
});
// Van
Breadcrumbs::register('van.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ข้อมูลรถตู้', route('van.index'));
});
Breadcrumbs::register('van.create', function ($breadcrumbs) {
    $breadcrumbs->parent('van.index');
    $breadcrumbs->push('เพิ่มข้อมูลรถตู้', route('van.create'));
});
Breadcrumbs::register('van.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('van.index');
    $breadcrumbs->push('แก้ไขข้อมูลรถตู้', route('van.edit', $id));
});
Breadcrumbs::register('van.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('van.index');
    $breadcrumbs->push('รายละเอียดรถตู้', route('van.show', $id));
});
Breadcrumbs::register('van.detail', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('reservation.create');
    $breadcrumbs->push('ปฏิทิน', route('van.detail', $id));
});
// Reservation
Breadcrumbs::register('reservation.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ข้อมูลการจอง', route('reservation.index'));
});
Breadcrumbs::register('reservation.create', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('จองรถตู้', route('reservation.create'));
});
Breadcrumbs::register('reservation.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('reservation.index');
    $breadcrumbs->push('แก้ไขข้อมูลการจอง', route('reservation.edit', $id));
});
Breadcrumbs::register('reservation.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('reservation.index');
    $breadcrumbs->push('รายละเอียดการจอง', route('reservation.show', $id));
});
Breadcrumbs::register('reservation.status', function ($breadcrumbs, $status) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ข้อมูลการจอง', route('reservation.status', $status));
});
// Post
Breadcrumbs::register('post.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('บทความ', route('post.index'));
});
Breadcrumbs::register('post.create', function ($breadcrumbs) {
    $breadcrumbs->parent('post.index');
    $breadcrumbs->push('เพิ่มบทความ', route('post.create'));
});
Breadcrumbs::register('post.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('post.index');
    $breadcrumbs->push('แก้ไขบทความ', route('post.edit', $id));
});
Breadcrumbs::register('post.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('post.index');
    $breadcrumbs->push('รายละเอียดบทความ', route('post.show', $id));
});