<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Van extends Model
{
    protected $guarded = [];

    public function Driver()
    {
        return $this->hasOne(Driver::class);
    }

    public function Reservation()
    {
        return $this->hasMany(Reservation::class);
    }

}
