<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $guarded = [];
    
    public function Van()
    {
        return $this->belongsTo(Van::class);
    } 

    public function Reservation()
    {
        return $this->hasMany(Reservation::class);
    }
}
