<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    public function Van()
    {
        return $this->belongsTo(Van::class);
    } 

    public function Driver()
    {
        return $this->belongsTo(Driver::class);
    } 

    public function Customer()
    {
        return $this->belongsTo(Customer::class);
    } 

    public function Payment()
    {
        return $this->belongsTo(Payment::class);
    }
}
