<?php
namespace Libraries\DateThaiLibrary;

class DateThaiLibrary
{
    public static function ThaiDate($date, $short = TRUE, $day = FALSE)
    {
        if($short == false)
        {
            $thai_month = ["1"=>"มกราคม ", "2"=>"กุมภาพันธ์ ", "3"=>"มีนาคม ", "4"=>"เมษายน ","5"=>"พฤษภาคม " , "6"=>"มิถุนายน ", "7"=>"กรกฎาคม ","8"=>"สิงหาคม " ,"9"=>"กันยายน ","10"=>"ตุลาคม ","11"=>"พฤศจิกายน ","12"=>"ธันวาคม "];
            $thai_day   = ["อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์"];
            $day        = "วัน ".$thai_day[date("w",strtotime($date))]." ที่ ";
        } else {
            $thai_month = ["1"=>" ม.ค.","2"=>" ก.พ.","3"=>" มี.ค.","4"=>" เม.ย.","5"=>" พ.ค.", "6"=>" มิ.ย.", "7"=>" ก.ค.","8"=>" ส.ค.","9"=>" ก.ย.","10"=>" ต.ค.","11"=>" พ.ย.","12"=>" ธ.ค."];
            // $thai_day   = ["อา","จ","อ","พ","พฤ","ศ","ส"];
            $day        = NULL;
        }
        return $day.date("j",strtotime($date))." ".$thai_month[date("n",strtotime($date))]." ".(date("Y",strtotime($date))+543);
    }
    public static function DateTimeTh($strDate, $br = TRUE)
  	{
      $br  = ($br == TRUE)  ? "<br>" : " <br> เวลา ";
  		$strYear = date("Y",strtotime($strDate))+543;
  		$strMonth= date("n",strtotime($strDate));
  		$strDay= date("j",strtotime($strDate));
  		$strHour= date("H",strtotime($strDate));
  		$strMinute= date("i",strtotime($strDate));
  		$strSeconds= date("s",strtotime($strDate));
  		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
  		$strMonthThai=$strMonthCut[$strMonth];
          // return "$strDay/$strMonth/$strYear <br> $strHour : $strMinute : $strSeconds";
      return  $strDay." ".$strMonthThai." ".$strYear ." ".$br." ". $strHour ." : ". $strMinute;
      }

    public static function Month($short = TRUE)
  	{
          if($short == false)
          {
              $month = ["1"=>"มกราคม ", "2"=>"กุมภาพันธ์ ", "3"=>"มีนาคม ", "4"=>"เมษายน ","5"=>"พฤษภาคม " , "6"=>"มิถุนายน ", "7"=>"กรกฎาคม ","8"=>"สิงหาคม " ,"9"=>"กันยายน ","10"=>"ตุลาคม ","11"=>"พฤศจิกายน ","12"=>"ธันวาคม "];
          }
          else
          {
              $month = ["1"=>" ม.ค.","2"=>" ก.พ.","3"=>" มี.ค.","4"=>" เม.ย.","5"=>" พ.ค.", "6"=>" มิ.ย.", "7"=>" ก.ค.","8"=>" ส.ค.","9"=>" ก.ย.","10"=>" ต.ค.","11"=>" พ.ย.","12"=>" ธ.ค."];
          }
          return  $month;
    }
    public static function GetMonth($date)
    {
        $thai_month = ["1"=>"มกราคม ", "2"=>"กุมภาพันธ์ ", "3"=>"มีนาคม ", "4"=>"เมษายน ","5"=>"พฤษภาคม " , "6"=>"มิถุนายน ", "7"=>"กรกฎาคม ","8"=>"สิงหาคม " ,"9"=>"กันยายน ","10"=>"ตุลาคม ","11"=>"พฤศจิกายน ","12"=>"ธันวาคม "];
        return $thai_month[date("n",strtotime($date))]." ".(date("Y",strtotime($date))+543);
    }
    
}
