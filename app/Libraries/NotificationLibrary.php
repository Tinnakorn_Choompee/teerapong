<?php
namespace Libraries\NotificationLibrary;
use App\Models\Reservation;

class NotificationLibrary
{
    public static function Read()
    {
        return Reservation::where('status', 1)->where('read', 0)->count();
    }
}