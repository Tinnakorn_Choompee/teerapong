<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class DriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'code'      => 'required|unique:drivers,code,'.$request->id,
            'name'      => 'required|unique:drivers,name,'.$request->id,
            'telephone' => 'required|numeric|unique:drivers,telephone,'.$request->id,
            'image'     => 'nullable|mimes:jpeg,png,jpg',
        ];
    }

    public function messages()
    {
        return [
            'code.unique'       => 'มีรหัสนี้แล้ว',
            'name.unique'       => 'มีชื่อนี้แล้ว',
            'telephone.numeric' => 'กรุณากรอกเบอร์โทรเป็นตัวเลข',
            'telephone.unique'  => 'มีเบอร์โทรนี้แล้ว',
        ];
    }
}
