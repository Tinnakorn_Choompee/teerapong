<?php

namespace App\Http\Controllers;

use App\Models\Driver;
use App\Models\Van;
use App\Http\Requests\DriverRequest;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('driver.index')->withDriver(Driver::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('driver.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DriverRequest $request)
    {
        $driver = new Driver();

        if($request->hasFile('image')) {
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(250, 250);
            $image_resize->save('images/backend/drivers/'.$name);
            $driver->image = $name;
        } else {
            $driver->image = "driver.png";
        }
        $driver->code      = $request->code;
        $driver->van_id    = 0;
        $driver->prename   = $request->prename;
        $driver->name      = $request->name;
        $driver->surname   = $request->surname;
        $driver->telephone = $request->telephone;
        $driver->save();
        return redirect()->route('driver.index')->with('success', 'Save Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        return view('driver.show')->withDriver($driver);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function edit(Driver $driver)
    {
        return view('driver.edit')->withDriver($driver);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function update(DriverRequest $request, Driver $driver)
    {
        if($request->hasFile('image')) {
            // Old Image
            $request->edit_image == 'driver.png' ? : File::delete('images/backend/drivers/'.$request->edit_image);
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(250, 250);
            $image_resize->save('images/backend/drivers/'.$name);
            $driver->image = $name;
        } else {
            $driver->image = $request->edit_image;
        }

        if(empty($request->van_id)) {
            $driver->van_id = $request->van_edit;  
            $van            = Van::where('driver_id', $driver->id)->first();
            $van->driver_id = $request->van_edit;   
            $van->save();
        } else {
            $driver->van_id = $request->van_edit;
        }

        $driver->code      = $request->code;
        $driver->prename   = $request->prename;
        $driver->name      = $request->name;
        $driver->surname   = $request->surname;
        $driver->telephone = $request->telephone;
        $driver->save();
        return redirect()->route('driver.index')->with('update', 'Update Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        Driver::destroy($driver->id);

        $van   = Van::where('driver_id',$driver->id)->first();
        $van->driver_id = 0;
        $van->save();

        return redirect()->route('driver.index')->with('delete', 'Delete Successfully!');
    }
}
