<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Models\Van;
use App\Models\Payment;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Carbon;
use Auth;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasRole('Admin')):
            return view('reservation.index')->withReservations(Reservation::all());
        else : 
            return view('reservation.index')->withReservations(Reservation::where('customer_id', Auth::user()->customer_id)->orderBy('created_at', 'DESC')->get());
        endif;
    }

    /**
     * Show the form for creating a new resource.WW
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // โค้ดนี้เอาใช้ เปรียบเทียบ ว่า วันนี้ จอง อยู่ใน ช่วงเวลาหรือไม่
        // $paymentDate = date('Y-m-d');
        // $paymentDate = date('Y-m-d', strtotime($paymentDate));;
        // //echo $paymentDate; // echos today! 
        // $contractDateBegin = date('Y-m-d', strtotime("01/01/2001"));
        // $contractDateEnd = date('Y-m-d', strtotime("01/01/2012"));
    
        // if (($paymentDate > $contractDateBegin) && ($paymentDate < $contractDateEnd))
        // {
        //   echo "is between";
        // }
        // else
        // {
        //   echo "NO GO!";  
        // }

        // switch(isset($request->type)) :
        //     case 1 : 
        //     $van = Van::where('status', $request->type)->get();
        //     break;
        //     case 2 : 
        //     $van = Van::where('status', $request->type)->get();
        //     break;
        //     default :
        //     $van = Van::with('reservation')->get();
        // endswitch;
        $van = Van::with('reservation')->get();
        return view('reservation.create')->withVans($van);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = ['date.required' => 'กรุณาระบุวันที่จอง'];
        $this->validate($request, ['date' => 'required',], $message);
        if($request->type[$request->van_id] == 1) :
            $date = $request->date;
            $day  = 1;
            $rate = $request->van_rate;
            $start_date = $request->date;
            $end_date   = $request->date;
        else :
            $date   = explode(" ถึง ",$request->date);
            $end    = Carbon::parse($date[1]);
            $start  = Carbon::parse($date[0]);
            $length = $end->diffInDays($start);
            $day    = $length + 1;
            $rate   = $request->van_rate * $day;
            $start_date = $date[0];
            $end_date   = $date[1];
        endif;
        $reservation                   = new Reservation;
        $reservation->van_id           = $request->van_id;
        $reservation->driver_id        = $request->driver_id;
        $reservation->customer_id      = $request->customer_id;
        $reservation->payment_id       = 0;
        $reservation->type             = $request->type[$request->van_id];
        $reservation->reservation_date = Carbon::now();
        $reservation->start_date       = $start_date;
        $reservation->end_date         = $end_date;
        $reservation->numdate          = $day;
        $reservation->price            = $rate;
        $reservation->status           = 0;
        $reservation->read             = 0;
        $reservation->save();
        return redirect()->route('reservation.status', 0)->with('success', 'Save Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        return view('reservation.show')->withReservation($reservation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        return view('reservation.edit')->withReservation($reservation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        $message = ['date.required' => 'กรุณาระบุวันที่จอง'];
        $this->validate($request, ['date' => 'required',], $message);
        if($request->type[$reservation->id] == 1) :
            $date = $request->date;
            $day  = 1;
            $rate = $request->van_rate;
            $start_date = $request->date;
            $end_date   = $request->date;
        else :
            $date   = explode(" ถึง ",$request->date);
            $end    = Carbon::parse($date[1]);
            $start  = Carbon::parse($date[0]);
            $length = $end->diffInDays($start);
            $day    = $length + 1;
            $rate   = $request->van_rate * $day;
            $start_date = $date[0];
            $end_date   = $date[1];
        endif;
        $reservation->van_id           = $request->van_id;
        $reservation->driver_id        = $request->driver_id;
        $reservation->customer_id      = $request->customer_id;
        $reservation->payment_id       = 0;
        $reservation->type             = $request->type[$reservation->id];
        $reservation->reservation_date = Carbon::now();
        $reservation->start_date       = $start_date;
        $reservation->end_date         = $end_date;
        $reservation->numdate          = $day;
        $reservation->price            = $rate;
        $reservation->status           = 0;
        $reservation->read             = 0;
        $reservation->save();
        return redirect()->route('reservation.status', 0)->with('update', 'Update Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        Reservation::destroy($reservation->id);
        return redirect()->route('reservation.status', $reservation->status)->with('delete', 'Delete Successfully!');
    }

    public function type(Request $request) 
    {
        return redirect()->route('reservation.create', ['type'=> $request->type]);
    }

    public function payment(Request $request)
    {
        $message = ['date.required' => 'กรุณาระบุวันที่โอน', 'time.required' => 'กรุณาระบุเวลาที่โอน', 'total' => 'กรุณาระบุจำนวนเงินที่โอน'];
        $this->validate($request, ['date' => 'required', 'time' => 'required' , 'total' => 'required'], $message);
        $payment = New Payment;
        $payment->reservation_id = $request->reservation_id;
        $payment->date  = $request->date;
        $payment->time  = $request->time;
        $payment->total = $request->total;
        if($request->hasFile('image')) {
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(500, 650);
            $image_resize->save('images/backend/payment/'.$name);
            $payment->image = $name;
        } else {
            $payment->image = "payment.png";
        }
        $payment->save();

        $reservation = Reservation::find($request->reservation_id);
        $reservation->payment_id = $payment->id;
        $reservation->status = 1;
        $reservation->save();
        // Nexmo::message()->send([
        //     'to'   => '66918595385',
        //     'from' => 'Teerapong Tour',
        //     'text' => 'Payment Total '.$payment->total.' Bath !!  DateTime :'.Carbon::parse($payment->date)->format('d/m/Y').'-'.$payment->time
        // ]);
        return redirect()->route('reservation.show', $request->reservation_id)->with('payment', 'Save Successfully!');
    }

    public function status($status)
    {
        if(Auth::user()->hasRole('Admin')):
            foreach(Reservation::all() as $rs):
                if($rs->status == 0):
                    if(Carbon::now() > Carbon::parse($rs->reservation_date)->addHour(3)):
                        Reservation::destroy($rs->id);
                    endif;
                endif;
            endforeach;
            $reservation = Reservation::where('status', '0')->count(); // ขอมูลการจอง
            $payment     = Reservation::where('status', '1')->count(); // รอการอนุมัติ
            $apporve     = Reservation::where('status', '2')->count(); // อนุมัติสำเร็จ
            $not         = Reservation::where('status', '3')->count(); // ไม่อนุมัติ
            return view('reservation.index')
            ->withReservations(Reservation::where('status', $status)
            ->orderBy('created_at', 'DESC')->get())
            ->withStatus($status)
            ->withReservation($reservation)
            ->withPayment($payment)
            ->withApporve($apporve)
            ->withNot($not);
        else :
            $reservation = Reservation::where('customer_id', Auth::user()->customer->id)->where('status', '0')->count(); // ขอมูลการจอง
            $payment     = Reservation::where('customer_id', Auth::user()->customer->id)->where('status', '1')->count(); // รอการอนุมัติ
            $apporve     = Reservation::where('customer_id', Auth::user()->customer->id)->where('status', '2')->count(); // อนุมัติสำเร็จ
            $not         = Reservation::where('customer_id', Auth::user()->customer->id)->where('status', '3')->count(); // ไม่อนุมัติ
            return view('reservation.index')
            ->withReservations(Reservation::where('customer_id', Auth::user()->customer_id)
            ->where('status', $status)->orderBy('created_at', 'DESC')->get())
            ->withStatus($status)
            ->withReservation($reservation)
            ->withPayment($payment)
            ->withApporve($apporve)
            ->withNot($not);
        endif;
    }

    public function apporve(Request $request, $id)
    {
       $reservation = Reservation::find($id);
       switch ($request->apporve) :
        case 0:
            $reservation->apporve = $request->apporve;
            $reservation->note    = $request->note;
            $reservation->status  = 3;
            $reservation->save();
            return redirect()->route('reservation.status', $reservation->status)->with('not', 'Not Successfully!');
        break;
        case 1:
            $reservation->apporve = $request->apporve;
            $reservation->status  = 2;
            $reservation->save();
            return redirect()->route('reservation.status', $reservation->status)->with('apporve', 'Apporve Successfully!');
        break;
       endswitch;
     
    }
}
