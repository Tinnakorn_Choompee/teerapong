<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::orderby('created_at', 'DESC')->get();
        return view('post.index')->withPost($post);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post();
        $post->title    = $request->title;
        $post->subtitle = $request->subtitle;
        $post->body     = $request->body;
        if($request->hasFile('image')) {
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(650 ,650);
            $image_resize->save('images/backend/posts/'.$name);
            $post->image = $name;
        } else {
            $post->image = "preview.png";
        }
        $post->save();
        return redirect()->route('post.index')->with('success', 'Save Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('post.edit')->withPost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $post->title    = $request->title;
        $post->subtitle = $request->subtitle;
        $post->body     = $request->body;
        if($request->hasFile('image')) 
        {
            // Old Image
            $request->edit_image == 'preview.png' ? : File::delete('images/backend/posts/'.$request->edit_image);
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(650 ,650);
            $image_resize->save('images/backend/posts/'.$name);
            $post->image = $name;
        } else {
            $post->image = $request->edit_image;
        } 
        $post->save();
        return redirect()->route('post.index')->with('update', 'Update Successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->image == 'preview.png' ? : File::delete('images/backend/posts/'.$post->image);
        Post::destroy($post->id);
        return redirect()->route('post.index')->with('delete', 'Delete Successfully!');
    }
}
