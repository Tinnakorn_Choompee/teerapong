<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;    
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserRequest;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class UserController extends Controller
{
    public function index()
    {
        return view('user.index')
            ->withUser(User::where('customer_id', 0)->get())
            ->withRole(Role::all());
    }

    public function create()
    {
        return view('user.create');
    }

    public function show(User $user)
    {
        return view('user.show')->withUser($user);
    }

    public function store(UserRequest $request)
    {
        $user = new User();

        if($request->hasFile('image')) {
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(250, 250);
            $image_resize->save('images/backend/users/'.$name);
            $user->image = $name;
        } else {
            $user->image = "user.png";
        }

        $user->customer_id = 0;
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->save();
        $user->roles()->attach(Role::where('name', "Admin")->first());
        return redirect()->route('user.index')->with('success', 'Save Successfully!');
    }

    public function edit(User $user)
    {
        return view('user.edit', compact('user'));
    }

    public function update(UserRequest $request, User $user)
    {
        $user = User::find($user->id);
        $user->customer_id = 0;
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);

        if($request->hasFile('image')) 
        {
            // Old Image
            $request->edit_image == 'user.png' ? : File::delete('images/backend/users/'.$request->edit_image);
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(250, 250);
            $image_resize->save('images/backend/users/'.$name);
            $user->image = $name;
        } else {
            $user->image = $request->edit_image;
        } 

        $user->save();
        return redirect()->route('user.index')->with('update', 'Update Successfully!');
    }

    public function destroy(User $user)
    {
        $user->image == 'user.png' ? : File::delete('images/backend/users/'.$user->image);
        $user->roles()->detach(Role::where('name', "Admin")->first()); 
        User::destroy($user->id);
        return redirect()->route('user.index')->with('delete', 'Delete Successfully!');
    }

    public function profile(User $user)
    {
        if($user->id == Auth::user()->id):
            return view('user.show')->withUser($user);
        else:
            return view('404');
        endif;
    }

    public function edit_profile(User $user)
    {
        if($user->id == Auth::user()->id):
            return view('user.edit_profile')->withUser($user);
        else:
            return view('404');
        endif;
    }

    public function update_profile(UserRequest $request, $id)
    {
        $user = User::find($id);
        $user->customer_id = 0;
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);

        if($request->hasFile('image')) 
        {
            // Old Image
            $request->edit_image == 'user.png' ? : File::delete('images/backend/users/'.$request->edit_image);
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(250, 250);
            $image_resize->save('images/backend/users/'.$name);
            $user->image = $name;
        } else {
            $user->image = $request->edit_image;
        } 

        $user->save();
        return redirect()->route('user.show', $user->id)->with('update', 'Update Successfully!');
    }

    public function password(Request $request)
    {
        $message = [
            'same' => 'พาสเวิร์ดยืนยันไม่ตรงกัน',
            'min'  => 'กรุณากรอกรหัสผ่าน 6 ตัวขึ้นไป',
            'max'  => 'กรุณากรอกรหัสผ่านเท่ากับ 12 ตัว หรือ น้อยกว่า 12 ตัว',
        ];
        $request->validate(['password_user' => 'required|min:6|max:12|same:password_confirmation'], $message);
        $user = User::find($request->id);
        $user->password = bcrypt($request->password_user);
        $user->save();
        return redirect('/home')->with('password', 'Change Successfully!');
    }
}
