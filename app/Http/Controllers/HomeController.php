<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasRole('Admin')):
            $reservation = Reservation::where('status', '0')->count(); // ขอมูลการจอง
            $payment     = Reservation::where('status', '1')->count(); // รอการอนุมัติ
            $apporve     = Reservation::where('status', '2')->count(); // อนุมัติสำเร็จ
            $history     = Reservation::where('status', '3')->count(); // ประวัติการชำระ
            return view('home', compact('reservation', 'payment', 'apporve', 'history'));
        else:
            $reservation = Reservation::where('customer_id', Auth::user()->customer->id)->where('status', '0')->count(); // ขอมูลการจอง
            $payment     = Reservation::where('customer_id', Auth::user()->customer->id)->where('status', '1')->count(); // รอการอนุมัติ
            $apporve     = Reservation::where('customer_id', Auth::user()->customer->id)->where('status', '2')->count(); // อนุมัติสำเร็จ
            $history     = Reservation::where('customer_id', Auth::user()->customer->id)->where('status', '3')->count(); // ประวัติการชำระ
            return view('home', compact('reservation', 'payment', 'apporve', 'history'));
        endif;
    }
}
