<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\User;
use App\Models\Role;  
use App\Http\Requests\CustomerRequest;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $customer = Customer::with('user')->get();
       return view('customer.index')->withCustomer($customer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $customer            = new Customer;
        $customer->name      = $request->name;
        $customer->telephone = $request->telephone;
        $customer->gender    = $request->gender;
        $customer->address   = $request->address;
        $customer->save();

        $user = new User;
        $user->customer_id  = $customer->id;
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->username     = $request->username;
        $user->password     = bcrypt($request->password);
        $user->image        = "user.png";
        $user->save();
        $user->roles()->attach(Role::where('name', "User")->first());
        return redirect()->route('login')->with('register', 'Registed Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        if($customer->user->id == Auth::user()->id || Auth::user()->hasRole('Admin')):
            return view('customer.show')->withCustomer($customer);
        else:
            return view('404');
        endif;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view('customer.edit')->withCustomer($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, Customer $customer)
    {
        $customer->name      = $request->name;
        $customer->telephone = $request->telephone;
        $customer->gender    = $request->gender;
        $customer->address   = $request->address;
        $customer->save();

        $user               = User::where('customer_id', $customer->id)->first();
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->username     = $request->username;
        $user->password     = bcrypt($request->password);

        if($request->hasFile('image')) 
        {
            // Old Image
            $request->edit_image == 'user.png' ? : File::delete('images/backend/users/'.$request->edit_image);
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(250, 250);
            $image_resize->save('images/backend/users/'.$name);
            $user->image = $name;
        } else {
            $user->image = $request->edit_image;
        } 

        $user->save();
        
        return redirect()->route('customer.index')->with('update', 'Update Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->image == 'user.png' ? : File::delete('images/backend/users/'.$customer->image);
        $user = User::where('customer_id', $customer->id)->first();
        $user->roles()->detach(Role::where('name', "Admin")->first()); 
        User::destroy($user->id);
        Customer::destroy($customer->id);
        return redirect()->route('customer.index')->with('delete', 'Delete Successfully!');
    }

    public function edit_profile(Customer $customer)
    {
        if($customer->user->id == Auth::user()->id):
            return view('customer.edit_profile')->withCustomer($customer);  
        else:
            return view('404');
        endif;
    }

    public function update_profile(CustomerRequest $request, Customer $customer)
    {
        $customer->name      = $request->name;
        $customer->telephone = $request->telephone;
        $customer->gender    = $request->gender;
        $customer->address   = $request->address;
        $customer->save();

        $user               = User::where('customer_id', $customer->id)->first();
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->username     = $request->username;
        $user->password     = bcrypt($request->password);
        
        if($request->hasFile('image')) 
        {
            // Old Image
            $request->edit_image == 'user.png' ? : File::delete('images/backend/users/'.$request->edit_image);
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(250, 250);
            $image_resize->save('images/backend/users/'.$name);
            $user->image = $name;
        } else {
            $user->image = $request->edit_image;
        } 

        $user->save();
        
        return redirect()->route('customer.show', $customer->id)->with('update', 'Update Successfully!');
    }
}
