<?php

namespace App\Http\Controllers;

use App\Models\Van;
use App\Models\Driver;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use DB;
use File;

class VanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('van.index')->withVan(Van::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $driver = Driver::where('van_id', 0)->select(DB::raw("CONCAT(prename,' ',name,' ',surname) AS fullname, id"))->pluck('fullname','id'); 
        return view('van.create')->withDriver($driver);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $van = new Van();
        if($request->hasFile('image')) {
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(250, 250);
            $image_resize->save('images/backend/vans/'.$name);
            $van->image = $name;
        } else {
            $van->image = "van.png";
        }
        $van->driver_id = $request->driver;
        $van->number    = $request->number;
        $van->brand     = $request->brand;
        $van->model     = $request->model;
        $van->status    = 0;
        $van->rate      = $request->rate;
        $van->save();

        $driver         = Driver::find($request->driver);
        $driver->van_id = $van->id;
        $driver->save();
        // Status :: 0 => ว่าง , 1 => ไม่ว่าง
        return redirect()->route('van.index')->with('success', 'Save Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Van  $van
     * @return \Illuminate\Http\Response
     */
    public function show(Van $van)
    {
        return view('van.show')->withVan($van);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Van  $van
     * @return \Illuminate\Http\Response
     */
    public function edit(Van $van)
    {
        $driver = Driver::where('van_id', 0)->select(DB::raw("CONCAT(prename,' ',name,' ',surname) AS fullname, id"))->pluck('fullname','id'); 
        return view('van.edit')->withVan($van)->withDriver($driver);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Van  $van
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Van $van)
    {
        if($request->hasFile('image')) {
            // Old Image
            $request->edit_image == 'van.png' ? : File::delete('images/backend/vans/'.$request->edit_image);
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(250, 250);
            $image_resize->save('images/backend/vans/'.$name);
            $van->image = $name;
        } else {
            $van->image = $request->edit_image;
        }
        $van->number    = $request->number;
        $van->brand     = $request->brand;
        $van->model     = $request->model;
        $van->status    = 0;
        $van->rate      = $request->rate;
        $van->driver_id = $request->driver;
        $van->save();

        $driver         = Driver::find($request->driver);
        $driver->van_id = $van->id;
        $driver->save();

        return redirect()->route('van.index')->with('update', 'Update Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Van  $van
     * @return \Illuminate\Http\Response
     */
    public function destroy(Van $van)
    {
        Van::destroy($van->id);

        $driver         = Driver::where('van_id',$van->id)->first();
        $driver->van_id = 0;
        $driver->save();

        return redirect()->route('van.index')->with('delete', 'Delete Successfully!');
    }

    public function detail($id)
    {
        return view('van.detail')->withVan(Van::find($id));
    }
}
