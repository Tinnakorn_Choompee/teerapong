@extends('layouts.app') 
@section('title', 'แก้ไขบทความ') 
@section('styles')
<!-- Summernote - text editor -->
{{ Html::style('plugin/bootstrap-wysihtml5/bootstrap3-wysihtml5.css') }}
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>
@endsection
@section('content')
       <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary title-header"> แก้ไขบทความ  </h3> </div>
                    @include('layouts.Backend.breadcrumb')
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-primary">
                            <div class="card-body">
                                {!! Form::model($post , ['route' => ['post.update', $post->id], 'files'=>TRUE , 'method'=>'PATCH']) !!}
                                {!! Form::hidden('edit_image', $post->image) !!}
                                    <div class="form-body">
                                        <h3 class="card-title m-t-15 font">ข้อมูลบทความ</h3>
                                        <hr>
                                        <div class="row p-t-20 justify-content-md-center">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    {!! Form::label('title', 'หัวข้อ', ['class'=>'label_font']) !!}
                                                    {!! Form::text('title', NULL, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('title'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('title') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row p-t-20 justify-content-md-center">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    {!! Form::label('subtitle', 'หัวข้อย่อย', ['class'=>'label_font']) !!}
                                                    {!! Form::text('subtitle', NULL, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('subtitle'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('subtitle') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row p-t-20 justify-content-md-center">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    {!! Form::label('image', 'รูปภาพ', ['class'=>'label_font']) !!}
                                                    {!! Form::file('image', ['class'=>'form-control', 'id' => 'img']); !!}
                                                    @if ($errors->has('image'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('image') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    {{ Html::image('images/backend/posts/'.$post->image, "User",['width'=>'250','height'=>'150', 'id'=>'blah']) }}
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row p-t-20 justify-content-md-center">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    {!! Form::label('sub_title', 'รายละเอียดบทความ', ['class'=>'label_font']) !!}
                                                    <textarea name="body" class="textarea_editor form-control" rows="15" placeholder="Enter text ..." style="height:450px">{{ $post->body }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->  
                                    </div>
                                    <div class="form-action text-center">
                                        <button type="submit" class="btn btn-success label_font"> <i class="fa fa-check"></i> บันทึก</button>
                                        <button onclick="goBack()" class="btn btn-inverse label_font">ยกเลิก</button>
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
        </div>
        <!-- End Page wrapper  -->
@endsection 
@push('scripts')
<!-- Summernote - text editor -->
{{ Html::script('plugin/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}
{{ Html::script('plugin/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}
{{ Html::script('plugin/bootstrap-wysihtml5/wysihtml5-init.js') }}
<script>
    $('.summernote').wysihtml5()
   
    function goBack() { window.history.back() }

    function readImage(input) {

    if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
    }
    }

    $("#img").change(function() {
        readImage(this);
    });

</script>
@endpush