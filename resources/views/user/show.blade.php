@extends('layouts.app') 
@section('title', 'ข้อมูลผู้ใช้งานระบบ') 
@section('content')
       <!-- Page wrapper  -->
       <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
              <h3 class="text-primary title-header">ข้อมูลผู้ใช้งานระบบ</h3> 
          </div>
          @include('layouts.Backend.breadcrumb')
        </div>
      <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div id="invoice" class="effect2">
                        <div id="invoice-mid">
                          <div class="row">
                            <div class="col-md-2 text-center">
                                {{ Html::image('images/backend/users/'.$user->image, "User",['width'=>'120','height'=>'120', 'class'=>'img-responsive img-circle']) }}
                            </div>
                            <div class="col-md-10">
                                <div class="invoice-info">
                                    <h2 class="label_font">ชื่อ</h2>
                                    <p  class="label_font">{{ $user->name }}<br>
                                    <h2 class="label_font">อีเมล์</h2>
                                    <p  class="label_font">{{ $user->email }}<br>
                                </div>
                                <div id="project">
                                    <h2 class="label_font">ชื่อเข้าใช้ระบบ</h2>
                                    <p  class="label_font">{{ $user->username }}</p>
                                    <br>
                                    <a class="btn btn-warning btn-edit font" style="font-size:18px" href="{{ route('user.edit_profile', $user->id) }}" data-toggle="tooltip" data-placement="bottom" title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i> แก้ไขข้อมูล </a>  
                                </div>
                            </div>
                          </div>
                        </div>
                        <!--End Invoice Mid-->
                    </div>
                    <!--End Invoice-->
                </div>
            </div>
            <!-- End PAge Content -->
        </div>
        <!-- End Container fluid  -->
    </div>
    <!-- End Page wrapper  -->
@endsection 
@push('scripts')
@if (session('update'))
<script>
    swal("Success!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
</script>
@endif
@endpush
   
   
