@extends('layouts.app') 
@section('title', 'ผู้ใช้งานระบบ | User') 
@section('content')
            <!-- Page wrapper  -->
            <div class="page-wrapper">
                    <!-- Bread crumb -->
                    <div class="row page-titles">
                        <div class="col-md-5 align-self-center">
                        <h3 class="text-primary title-header">ผู้ใช้งานระบบ</h3> </div>
                        @include('layouts.Backend.breadcrumb')
                    </div>
                    <!-- End Bread crumb -->
                    <!-- Container fluid  -->
                    <div class="container-fluid">
                        <!-- Start Page Content -->
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title font"> ตารางผู้ใช้งานระบบ  
                                            <a href="{{ route('user.create') }}" class="btn btn-info btn-rounded btn_employee font" style="font-size:22px"> 
                                                <i class="fa fa-user-plus" style="margin-right:10px;"></i> เพิ่มผู้ใช้งานระบบ
                                            </a>
                                        </h5>
                                        <div class="table-responsive m-t-40">
                                            <table id="example2" class="table table-bordered table-hover font">
                                                <thead>
                                                    <tr class="text-center">
                                                        <th width="7%">#</th>
                                                        <th width="15%">ชื่อ</th>
                                                        <th width="12  %">ชื่อเช้าใช้ระบบ</th>
                                                        <th width="15%">อีเมล์</th>
                                                        <th width="10%">รูปภาพ</th>
                                                        <th width="25%">ตัวเลือก</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr class="text-center">
                                                        <th width="7%">#</th>
                                                        <th width="15%">ชื่อ</th>
                                                        <th width="12%">ชื่อเช้าใช้ระบบ</th>
                                                        <th width="15%">อีเมล์</th>
                                                        <th width="10%">รูปภาพ</th>
                                                        <th width="25%">ตัวเลือก</th>
                                                    </tr>
                                                </tfoot>
                                                <tbody style="font-size:20px;" class="text-center">
                                                    @foreach($user as $k => $rs)
                                                        @empty($rs->hasRole('User'))
                                                        <tr>
                                                            <td class="text-center">{{ ++$k }}</td>
                                                            <td>{{ $rs->name }}</td>
                                                            <td>{{ $rs->username }}</td>
                                                            <td>{{ $rs->email }}</td>
                                                            <td width="20%" class="text-center">{{ Html::image('images/backend/users/'.$rs->image, $rs->image , ['width'=>'90','height'=>'80','class'=>'rounded']) }}</td>
                                                            <td class="text-center">
                                                                <a class="btn btn-warning btn-edit" href="{{ route('user.edit', $rs->id) }}" data-toggle="tooltip" data-placement="bottom" title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i></a>  
                                                                <button class="btn btn-danger btn-del"  data-id="{{ $rs->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button> 
                                                                {{ Form::open(['method' => 'DELETE', 'route' => ['user.destroy', $rs->id], 'id'=>'form-delete-'.$rs->id]) }} {{ Form::close() }}
                                                            </td>
                                                        </tr>
                                                        @endempty
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End PAge Content -->
                    </div>
                    <!-- End Container fluid  -->
                </div>
                <!-- End Page wrapper  -->
@endsection 
@push('scripts')
    <!-- DataTables -->
    {{ Html::script('js/backend/lib/datatables/jquery.dataTables.js') }}
    <!-- page script -->
    @if (session('success'))
        <script>
            swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('update'))
        <script>
            swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('delete'))
        <script>
            swal("Delete!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @endif
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.btn-del').on('click',function(){
        var id = $(this).data('id');
        swal({
                title: "Are you sure?",
                text: "ต้องการที่จะลบ ผู้ใช้งาน นี้ใช่หรือไม่ !!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then(willDelete => {
                if (willDelete) { 
                    $( "#form-delete-"+id ).submit();
                }
            });
        });

        $('[data-toggle="tooltip"]').tooltip(); 

        $(function () {
                $('#report').DataTable({ "dom": 'rtp'});
                $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        });
    </script>
@endpush