@extends('layouts.main')
@section('content')
<!-- Portfolio Section -->
<div id="portfolio">
  <div class="container">
    <div class="col-md-8 col-md-offset-2 section-title text-center">
      <h2>NEWS</h2>
      <p style="font-size:25px">ข่าวกิจกรรม</p>
    </div>
    <div class="clearfix"></div>
    <div class="row featurette">
        <div class="col-md-7 col-md-push-5">
          <h2 class="featurette-heading font" > {{ $post->title }} </h2>
          <p class="lead font" style="color:#ccc;"> {{ $post->subtitle }} </p>
          <hr>
          <p class="lead">{{ $post->body }}</p>
        </div>
        <div class="col-md-5 col-md-pull-7">
          {{ Html::image('images/backend/posts/'.$post->image, NULL , ['class'=>'featurette-image img-responsive center-block']) }}
        </div>
      </div>
  </div>
</div>
@endsection