@extends('layouts.app') 
@section('title', 'แก้ไขข้อมูลส่วนตัว') 
@section('content')
       <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary title-header">แก้ไขข้อมูลส่วนตัว</h3> 
                </div>
                @include('layouts.Backend.breadcrumb')
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-primary">
                            <div class="card-body">
                                    {!! Form::model($customer, ['route' => ['customer.update_profile', $customer->id], 'files'=>TRUE, 'method'=>'PATCH']) !!}
                                    {!! Form::hidden('id', $customer->user->id) !!}
                                    {!! Form::hidden('customer_id', $customer->id) !!}
                                    {{ csrf_field() }}
                                    <div class="form-body">
                                        <h3 class="card-title m-t-15 font">แก้ไขข้อมูลส่วนตัว</h3>
                                        <hr>
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('name', 'ชื่อ', ['class'=>'label_font']) !!}
                                                    {!! Form::text('name', NULL, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('name'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('name') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('name', 'อีเมล์', ['class'=>'label_font']) !!}
                                                    {!! Form::email('email', $customer->user->email, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('email'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('email') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('telephone', 'เบอร์โทร', ['class'=>'label_font']) !!}
                                                    {!! Form::text('telephone', NULL, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('telephone'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('telephone') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {{ Form::label('Gender', 'เพศ', ['class'=>'label_font']) }}
                                                    {{ Form::select('gender', ['1' => 'ชาย', '2' => 'หญิง'], NULL,['class'=>'form-control']) }}
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                         <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('username', 'ชื่อเข้าใช้งานระบบ', ['class'=>'label_font']) !!}
                                                    {!! Form::text('username', $customer->user->username, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('username'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('username') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('password', 'รหัสผ่าน', ['class'=>'label_font']) !!}
                                                    {!! Form::password('password', ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('password'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('password') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row p-t-20">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {{ Form::label('Address', 'ที่อยู่', ['class'=>'label_font']) }}
                                                    {{ Form::textarea('address', NULL ,['class'=>'form-control', 'rows'=> 4]) }}
                                                    @if ($errors->has('address'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('address') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">   
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('image', 'รูปภาพ', ['class'=>'label_font']) !!}
                                                    {!! Form::file('image', ['class'=>'form-control', 'id' => 'img']); !!}
                                                    {!! Form::hidden('edit_image', $customer->user->image) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 text-center">
                                                <div class="form-group">
                                                    {{ Html::image('images/backend/users/'.$customer->user->image, "User",['width'=>'120','height'=>'120', 'id'=>'blah']) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success label_font"> <i class="fa fa-check"></i> บันทึก</button>
                                        <a href="{{ route('customer.index') }}" class="btn btn-inverse label_font">ยกเลิก</a>
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
        </div>
        <!-- End Page wrapper  -->
@endsection 
@push('scripts')
<script>

    function readImage(input) {

    if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
    }
    }

    $("#img").change(function() {
        readImage(this);
    });

</script>
@endpush