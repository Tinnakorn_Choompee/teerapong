@extends('layouts.app') 
@section('title', 'ข้อมูลส่วนตัว') 
@section('content')
       <!-- Page wrapper  -->
       <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
              <h3 class="text-primary title-header">ข้อมูลส่วนตัว</h3> 
          </div>
          @include('layouts.Backend.breadcrumb')
        </div>
      <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div id="invoice" class="effect2">
                        <div id="invoice-mid">
                          <div class="row">
                            <div class="col-md-2 text-center">
                                {{ Html::image('images/backend/users/'.$customer->user->image, "User",['width'=>'120','height'=>'120', 'class'=>'img-responsive img-circle']) }}
                                @if(Auth::user()->hasRole('User'))    
                                <a class="btn btn-warning btn-edit font" style="font-size:18px;margin-top:10px" href="{{ route('customer.edit_profile', $customer->id) }}"><i class="fa fa-pencil"></i> แก้ไขข้อมูล </a>  
                                @endif
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="invoice-info">
                                            <h2 class="label_font">ชื่อ</h2>
                                            <p  class="label_font">{{ $customer->name }}</p>
                                            <h2 class="label_font">อีเมล์</h2>
                                            <p  class="label_font">{{ $customer->user->email }}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                    <div class="invoice-info">
                                            <h2 class="label_font">เพศ</h2>
                                            <p  class="label_font">{{ $customer->gender == 1 ? "ชาย" : "หญิง" }}</p>
                                            <h2 class="label_font">เบอร์โทร</h2>
                                            <p  class="label_font">{{ $customer->telephone }}</p>
                                        </div> 
                                    </div>
                                    <div class="col-md-4">
                                        <div id="project">
                                            <h2 class="label_font">ชื่อเข้าใช้ระบบ</h2>
                                            <p  class="label_font">{{ $customer->user->username }}</p>
                                            <h2 class="label_font">ที่อยู่</h2>
                                            <p  class="label_font">{{ $customer->address }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                        <!--End Invoice Mid-->
                    </div>
                    <!--End Invoice-->
                </div>
            </div>
            <!-- End PAge Content -->
        </div>
        <!-- End Container fluid  -->
    </div>
    <!-- End Page wrapper  -->
@endsection 
@push('scripts')
@if (session('update'))
<script>
    swal("Success!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
</script>
@endif
@endpush
   
   
