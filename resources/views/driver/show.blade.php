@extends('layouts.app') 
@section('title', 'ข้อมูลส่วนตัวคนขับ') 
@section('content')
       <!-- Page wrapper  -->
       <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
              <h3 class="text-primary title-header">ข้อมูลส่วนตัวคนขับ</h3> 
          </div>
          @include('layouts.Backend.breadcrumb')
        </div>
      <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div id="invoice" class="effect2">
                        <div id="invoice-mid">
                          <div class="row">
                            <div class="col-md-2 text-center">
                                {{ Html::image('images/backend/drivers/'.$driver->image, "User",['width'=>'120','height'=>'120', 'class'=>'img-responsive img-circle']) }}
                            </div>
                            <div class="col-md-10">
                                <div class="invoice-info">
                                    <h2 class="label_font">รหัสประจำตัวคนขับ</h2>
                                    <p  class="label_font">{{ $driver->code }}<br>
                                    <h2 class="label_font">ชื่อ</h2>
                                    <p  class="label_font">{{ $driver->prename == 1 ? "นาย " : "นาง " }}{{ $driver->name." ".$driver->surname }}<br>
                                </div>
                                <div id="project">
                                    <h2 class="label_font">เบอร์โทร</h2>
                                    <p  class="label_font">{{ $driver->telephone }}</p>
                                    <h2 class="label_font">เลขทะเบียนประจำรถตู้</h2>
                                    @if($driver->van_id == 0)
                                    <span class="label label-rouded label-success label_font" style="font-size:20px">ว่าง</span>
                                    @else
                                    <p  class="label_font">{{ $driver->van->number }}</p>
                                    @endif
                                   
                                </div>
                            </div>
                          </div>
                        </div>
                        <!--End Invoice Mid-->
                    </div>
                    <!--End Invoice-->
                </div>
            </div>
            <!-- End PAge Content -->
        </div>
        <!-- End Container fluid  -->
    </div>
    <!-- End Page wrapper  -->
@endsection 

   
   
