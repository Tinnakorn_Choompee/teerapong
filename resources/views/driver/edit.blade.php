@extends('layouts.app') 
@section('title', 'แก้ไขข้อมูลคนขับ') 
@section('styles')
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    .checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid green;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
</style>
@endsection
@section('content')
       <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary title-header">แก้ไขข้อมูลคนขับ</h3> 
                </div>
                @include('layouts.Backend.breadcrumb')
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-primary">
                            <div class="card-body">
                                    {!! Form::model($driver, ['route' => ['driver.update', $driver->id], 'files'=>TRUE, 'method'=>'PATCH']) !!}
                                    {!! Form::hidden('id', $driver->id) !!}
                                    {!! Form::hidden('van_edit', $driver->van_id) !!}
                                    {!! Form::hidden('edit_image', $driver->image) !!}
                                    <div class="form-body">
                                        <h3 class="card-title m-t-15 font">ข้อมูลคนขับ</h3>
                                        <hr>
                                        <div class="row p-t-20">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('code', 'รหัสประจำตัวคนขับ', ['class'=>'label_font']) !!}
                                                    {!! Form::number('code', NULL, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('code'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('code') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('prename', 'คำนำหน้า', ['class'=>'label_font']) !!}
                                                    {!! Form::select('prename', ['นาย' => 'นาย', 'นาง' => 'นาง'], $driver->prename ,['class'=>'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('name', 'ชื่อ', ['class'=>'label_font']) !!}
                                                    {!! Form::text('name', NULL, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('name'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('name') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('surname', 'นามสกุล', ['class'=>'label_font']) !!}
                                                    {!! Form::text('surname', NULL, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('surname'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('surname') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->  
                                        <div class="row">     
                                            <!--/span-->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('telephone', 'เบอร์โทร', ['class'=>'label_font']) !!}
                                                    {!! Form::tel('telephone', NULL, ['class'=>'form-control', 'required', 'maxlength'=> 10]) !!}
                                                    @if ($errors->has('telephone'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('telephone') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('image', 'รูปภาพ', ['class'=>'label_font']) !!}
                                                    {!! Form::file('image', ['class'=>'form-control', 'id' => 'img']); !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('van_id', 'ปรับสถานะขับรถ', ['class'=>'label_font']) !!}
                                                     <div class="checkbox">                                                    
                                                        <label style="font-size: 1.5em;margin-top:3px;">
                                                            <input type="checkbox" value="0" name="van_id">
                                                            <span class="cr"><i class="cr-icon fa fa-check label-success"></i></span>
                                                            <span class="font">ว่าง</span>  
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 text-center">
                                                <div class="form-group">
                                                    {{ Html::image('images/backend/drivers/'.$driver->image, "User",['width'=>'120','height'=>'120', 'id'=>'blah']) }}
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success label_font"> <i class="fa fa-check"></i> บันทึก</button>
                                        <a href="{{ route('driver.index') }}" class="btn btn-inverse label_font">ยกเลิก</a>
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
        </div>
        <!-- End Page wrapper  -->
@endsection 
@push('scripts')
<script>


    function readImage(input) {

    if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
    }
    }

    $("#img").change(function() {
        readImage(this);
    });

</script>
@endpush