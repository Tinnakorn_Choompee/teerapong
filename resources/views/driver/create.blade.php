@extends('layouts.app') 
@section('title', 'เพิ่มข้อมูลคนขับ') 
@section('styles')
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>
@endsection
@section('content')
       <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary title-header">เพิ่มข้อมูลคนขับ</h3> </div>
                    @include('layouts.Backend.breadcrumb')
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-primary">
                            <div class="card-body">
                                	{!! Form::open(['route' => 'driver.store', 'files'=>TRUE]) !!}
                                    <div class="form-body">
                                        <h3 class="card-title m-t-15 font">ข้อมูลคนขับ</h3>
                                        <hr>
                                        <div class="row p-t-20">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('code', 'รหัสประจำตัวคนขับ', ['class'=>'label_font']) !!}
                                                    {!! Form::number('code', NULL, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('code'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('code') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('prename', 'คำนำหน้า', ['class'=>'label_font']) !!}
                                                    {!! Form::select('prename', ['นาย' => 'นาย', 'นาง' => 'นาง'], 'นาย',['class'=>'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('name', 'ชื่อ', ['class'=>'label_font']) !!}
                                                    {!! Form::text('name', NULL, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('name'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('name') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('surname', 'นามสกุล', ['class'=>'label_font']) !!}
                                                    {!! Form::text('surname', NULL, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('surname'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('surname') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->  
                                        <div class="row">     
                                            <!--/span-->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('telephone', 'เบอร์โทร', ['class'=>'label_font']) !!}
                                                    {!! Form::tel('telephone', NULL, ['class'=>'form-control', 'required', 'maxlength'=> 10]) !!}
                                                    @if ($errors->has('telephone'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('telephone') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('image', 'รูปภาพ', ['class'=>'label_font']) !!}
                                                    {!! Form::file('image', ['class'=>'form-control', 'id' => 'img']); !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6 text-center">
                                                <div class="form-group">
                                                    {{ Html::image('images/backend/users/user.png', "User",['width'=>'120','height'=>'120', 'id'=>'blah']) }}
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success label_font"> <i class="fa fa-check"></i> บันทึก</button>
                                        <button onclick="goBack()" class="btn btn-inverse label_font">ยกเลิก</button>
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
        </div>
        <!-- End Page wrapper  -->
@endsection 
@push('scripts')
<script>
  function goBack() { window.history.back() }

    function readImage(input) {

    if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
    }
    }

    $("#img").change(function() {
        readImage(this);
    });

</script>
@endpush