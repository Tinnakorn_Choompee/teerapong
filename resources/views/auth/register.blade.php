@extends('layouts.app')
@section('title', 'ลงทะเบียน | Register')
@section('login')
<!-- Main wrapper  -->
<div class="unix-login">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="login-content card">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="login-form">
                        <h4 class="font" style="font-size:25px;">ลงทะเบียนสมาชิก</h4>
                        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    {{ Form::label('Name', 'ชื่อ', ['class'=>'label_font']) }}
                                    {{ Form::text('name', old('name') , [ 'class'=>'form-control', 'required', 'autofocus'] ) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('Email', 'อีเมล์', ['class'=>'label_font']) }}
                                    {{ Form::email('email', old('email') , [ 'class'=>'form-control', 'required'] ) }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    {{ Form::label('Telephone', 'เบอร์โทร', ['class'=>'label_font']) }}
                                    {{ Form::tel('telephone', old('telephone') , [ 'class'=>'form-control', 'required', 'maxlength'=> 10] ) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('Gender', 'เพศ', ['class'=>'label_font']) }}
                                    {{ Form::select('gender', ['1' => 'ชาย', '2' => 'หญิง'], '1',['class'=>'form-control']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('Address', 'ที่อยู่', ['class'=>'label_font']) }}
                                {{ Form::textarea('address', old('address') ,['class'=>'form-control', 'rows'=> 4]) }}
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    {{ Form::label('Username', 'ชื่อเข้าใช้งานระบบ', ['class'=>'label_font']) }}
                                    {{ Form::text('username', old('username') , [ 'class'=>'form-control', 'required'] ) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ Form::label('Password', 'พาสเวิร์ด', ['class'=>'label_font']) }}
                                    {{ Form::password('password', [ 'class'=>'form-control', 'required'] ) }}
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-flat m-b-20 m-t-20 label_font">
                                ลงทะเบียน <i class="fa fa-user m-l-10"></i>
                            </button>
                        </form>
                        <div class="row">
                            <div class="col">
                                <a href="{{ route('/') }}" class="btn btn-info m-b-10 m-t-10 float-left font"
                                    style="font-size:20px"> <i class="fa fa-chevron-circle-left m-r-10"></i>
                                    หน้าแรก</a>
                            </div>
                            <div class="col">
                                <a href="{{ route('login') }}" class="btn btn-info m-b-10 m-t-10 float-right font"
                                    style="font-size:20px"> เข้าสู่ระบบ <i class="fa fa-sign-in m-l-10"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Wrapper -->
@endsection