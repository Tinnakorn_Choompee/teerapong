@extends('layouts.app')
@section('title', 'เข้าสู่ระบบ | Login')
@section('login')
<div class="unix-login">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="login-content card">
                    <div class="login-form">    
                        <h4 class="title-header">ระบบจองรถตู้ VIP <br> บริษัท ธีรพงษ์ทัวน์ จำกัด</h4>
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <label>Username</label>
                                {{ Form::text('username', old('username') , ['id'=>'username', 'class'=>'form-control', 'placeholder'=>'Username', 'required', 'autofocus','title'=> 'กรอก Username เข้าใช้งาน', 'oninvalid'=>"this.setCustomValidity('กรุณากรอก Username เข้าใช้งาน')",'oninput'=>"setCustomValidity('')"] ) }}
                                @if ($errors->has('username'))
                                <span class="help-block text-danger">
                                    <strong class="font" style="font-size:18px !important;">{{ $errors->first('username') }}</strong>
                                </span>
                                @endif  
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Password</label>
                                {{ Form::password('password', ['id'=>'password','class' => 'form-control', 'placeholder'=>'Password', 'required', 'title'=> 'กรอก Password เข้าใช้งาน', 'oninvalid'=>"this.setCustomValidity('กรุณากรอก Password เข้าใช้งาน')",'oninput'=>"setCustomValidity('')"]) }}
                                @if ($errors->has('password'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary btn-flat m-b-20 m-t-20 label_font">  เข้าสู่ระบบ <i class="fa fa-sign-in m-l-10"></i></button>
                        </form>
                        <div class="row">
                            <div class="col">
                                <a href="{{ route('/') }}" class="btn btn-info m-b-10 m-t-10 float-left font" style="font-size:20px"> <i class="fa fa-chevron-circle-left m-r-10"></i> หน้าแรก</a>
                            </div>
                            <div class="col">
                                <a href="{{ route('register') }}" class="btn btn-info m-b-10 m-t-10 float-right font" style="font-size:20px"> ลงทะเบียน <i class="fa fa-user m-l-10"></i> </a>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    @if (session('register'))
        <script>
            swal("Registed!", "ลงทะเบียนสมาชิกเรียบร้อยแล้ว", "success",{button: false});
        </script>
    @endif
@endpush
