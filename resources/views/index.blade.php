@extends('layouts.main')
@section('content')
<!-- Services Section -->
<div id="services" class="text-center">
  <div class="container">
    <div class="col-md-8 col-md-offset-2 section-title">
      <h2>Services</h2>
      <p style="color:#fff;font-size:30px">บริการของเรา</p>
      <p style="font-size:25px !important">บริษัท ธีรพงษ์ จำกัด รับบริการรถตู้เช่า  VIP ในจังหวัดพะเยา เชียงราย ลำปาง และเชียงใหม่ และถือว่าเป็นอีกทางเลือกหนึ่งที่ผู้บริโภคเลือกใช้บริการในการเดินทางเพราะมีราคาเหมาะสม บริการเป็นกันเอง และมีหลายเส้นทางให้ใช้บริการ</p>
    </div>
    <div class="row">
      <div class="col-xs-6 col-md-3"> <i class="fa fa-couch"></i>
        <h4>seat</h4>
        <p class="detail-service">เบาะนั่งสามารถปรับได้ตามความต้องการของลูกค้า และจะกว้างกว่ารถตู้ปกติทำให้ลูกค้านั่งอย่างสบาย </p>
      </div>
      <div class="col-xs-6 col-md-3"> <i class="fa fa-music"></i>
        <h4>Movie & Music</h4>
        <p class="detail-service">ภายในรถตู้ VIP จะแต่งด้วยเครื่องเสียงที่มีคุณภาพ สามารถดูหนังฟังเพลงได้ตลอดการเดินทาง </p>
      </div>
      <div class="col-xs-6 col-md-3"> <i class="fa fa-wifi"></i>
        <h4>Wifi & GPS</h4>
        <p class="detail-service">รถตู้ทุกคันจะติดตั้ง WIFi และ GPS ลูกค้าสามารถใช้งานอินเตอร์เน็ตและสามารถเรียกเส้นทางหรือปลายทางที่ต้องการได้ </p>
      </div>
      <div class="col-xs-6 col-md-3"> <i class="fa fa-thumbs-up"></i>
        <h4>High security</h4>
        <p class="detail-service">มีความปลอดภัยสูงด้วยการควบคุมความเร็วจาก GPS ของทางขนส่งจึงเป็นที่นิยมมากในปัจจุบัน</p>
      </div>
    </div>
  </div>
</div>
<!-- About Section -->
<div id="about">
  <div class="container">
    <div class="col-md-8 col-md-offset-2 section-title text-center">
      <h2>About Us</h2>
      <p style="font-size:25px;">บริษัท ธีรพงษ์ จำกัด ตั้งอยู่บ้านเลขที่ 246 หมู่ 2 ตำบลแม่สุก อำเภอแม่ใจ จังหวัดพะเยา ได้ดำเนินธุรกิจการเดินรถตู้ VIP ในประเทศไทยโดยต้นทางที่สามารถรับบริการรถได้มีจังหวัดพะเยา เชียงราย ลำปาง และเชียงใหม่ </p>
    </div>
    <div class="row">
      <div class="col-xs-12 col-md-12"> 
        {{ Html::image('images/frontend/about.jpg', NULL , ['class'=>'img-responsive', 'style'=>'border-radius: 8px;height:450px;margin:0 auto;',]) }}
      </div>
    </div>
  </div>
</div>
<!-- Portfolio Section -->
<div id="portfolio">
  <div class="container">
    <div class="col-md-8 col-md-offset-2 section-title text-center">
      <h2>NEWS</h2>
      <p style="font-size:25px">ข่าวกิจกรรม</p>
    </div>
    <div class="clearfix"></div>
    <div class="row">
  
        <!-- Three columns of text below the carousel -->
        <div class="row text-center">
          @foreach($post as $rs)
            <div class="col-lg-4">
              {{ Html::image('images/backend/posts/'.$rs->image, NULL , ['class'=>'img-rounded', 'width'=>'250', 'height'=>'250']) }}
              <p style="font-size:25px;color:#fff" class="text-white">{{ $rs->title }}</p>
              <p style="font-size:22px;">{{ $rs->subtitle }}</p>
              <p ><a style="font-size:16px;" class="btn btn-default font" href="{{ route('news', $rs->id) }}" role="button"> อ่านต่อ  &raquo;</a></p>
            </div><!-- /.col-lg-4 -->
            @endforeach
        </div><!-- /.row -->
        
    </div><!--/row-->
  </div>
</div>
<!-- Team Section -->
<div id="team" class="text-center">
  <div class="container">
    <div class="col-md-8 col-md-offset-2 section-title">
      <h2>Drivers</h2>
      <p style="font-size:25px;">รายชื่อคนขับรถตู้</p>
    </div>
    <div id="row">
      @foreach($driver as $rs)
      <div class="col-md-3 col-sm-6 team">
        <div class="thumbnail"> 
          {{ Html::image('images/backend/drivers/'.$rs->image , NULL , ['class'=>'img-circle team-img']) }}
          <div class="caption">
            <h3 style="font-size:22px;">{{ $rs->prename == 1 ? "นาย " : "นาง " }} {{ $rs->name." ".$rs->surname }}</h3>
            <p style="font-size:22px;">ประจำรถตู้ A </p>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>
<!-- Contact Section -->
<div id="contact" class="text-center">
  <div class="overlay">
    <div class="container">
      <div class="col-md-8 col-md-offset-2 section-title">
        <h2>Contact Us</h2>
        <div class="col-md-5">
            <p class="text-right" style="font-size:25px;color:#fff"> Telephone :  </p>
            <p class="text-right" style="font-size:25px;color:#fff"> ID Line :  </p>
            <p class="text-right" style="font-size:25px;color:#fff"> E-MAIL :  </p>
        </div>
        <div class="col-md-7">
            <p class="text-left" style="font-size:25px;color:#fff"> 0655766331 </p>
            <p class="text-left" style="font-size:25px;color:#fff"> yo_narongrit </p>
            <p class="text-left" style="font-size:25px;color:#fff"> Yo2538narongrit@gmail.com </p>
        </div>
       
      </div>
    </div>
  </div>
</div>
@endsection
