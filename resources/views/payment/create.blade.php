    <!-- Modal -->
<div class="modal fade" id="reservation" role="dialog" aria-labelledby="reservationLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="margin-top:0%;">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="reservationLabel">แจ้งชำระเงิน</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top:0%;padding-left:20%;">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        {{ Form::open(['route'=>'reservation.payment', 'files'=> TRUE]) }}
        {{ Form::hidden('reservation_id', $reservation->id) }}
        <div class="modal-body">
            <div class="card-body">
                <div class="form-body">
                    <div class="row p-t-20">
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('date', 'วันที่โอนเงิน', ['class'=>'label_font']) !!}
                                {!! Form::text('date', NULL, ['class'=>'form-control date', 'required']) !!}
                                @if ($errors->has('date'))
                                    <small class="form-control-feedback text-danger"> {{ $errors->first('date') }} </small> 
                                @endif
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('time', 'เวลาที่โอนเงิน', ['class'=>'label_font']) !!}
                                {!! Form::text('time', NULL, ['class'=>'form-control time', 'required']) !!}
                                @if ($errors->has('time'))
                                    <small class="form-control-feedback text-danger"> {{ $errors->first('time') }} </small> 
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('total', 'ยอดเงิน', ['class'=>'label_font']) !!}
                                {!! Form::number('total', NULL, ['class'=>'form-control', 'required']) !!}
                                @if ($errors->has('total'))
                                    <small class="form-control-feedback text-danger"> {{ $errors->first('total') }} </small> 
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('image', 'หลักฐานการชำระเงิน', ['class'=>'label_font']) !!}
                                {!! Form::file('image', ['class'=>'form-control']); !!}
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary label_font" data-dismiss="modal">ยกเลิก</button>
          <button type="submit" class="btn btn-primary label_font">แจ้งชำระเงิน</button>
        </div>
        {{ Form::close() }}
      </div>
    </div>
