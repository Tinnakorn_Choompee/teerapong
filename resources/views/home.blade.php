@extends('layouts.app') 
@section('title', 'หน้าหลัก | Home') 
@section('styles') 
{{ Html::style('public/css/Backend/animate.css') }}
@endsection
 
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <div class="row page-titles">
        <div class="col-md-12 align-self-center">
            <h3 class="text-primary">Dashboard <span class="float-right font"> สวัสดี คุณ {{ Auth::user()->name }}</span> </h3>
        </div>
    </div>
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Admin Page Content -->
        <div class="row">
            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="far fa-address-card f-s-40 color-primary"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <a href="{{ route('reservation.status' , 0) }}">
                                <h2>{{ $reservation }}</h2>
                                <p class="m-b-0 label_font">ข้อมูลการจอง</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="far fa-money-bill-alt f-s-40 color-success"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <a href="{{ route('reservation.status' , 1) }}">
                                <h2>{{ $payment }}</h2>
                                <p class="m-b-0 label_font">แจ้งชำระเงิน</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="far fa-times-circle f-s-40 color-danger"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <a href="{{ route('reservation.status' , 3) }}">
                                <h2>{{ $history}}</h2>
                                <p class="m-b-0 label_font">ไม่อนุมัติ</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fas fa-dollar-sign f-s-40 color-warning"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <a href="{{ route('reservation.status' , 2) }}">
                                <h2>{{ $apporve }}</h2>
                                <p class="m-b-0 label_font">อนุมัติชำระเงิน</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <!-- End PAge Content -->

        <!-- Admin Page Content -->
        @if(Auth::user()->hasRole('Admin'))
        <div class="row bg-white m-l-0 m-r-0 box-shadow ">
            <!-- column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">MENU</h4>
                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('home') }}" class="hvr-pop">
                                        <i class="fa fa-tachometer" style="font-size:30px;"></i> 
                                        <p class="label_font">หน้าหลัก </p>     
                                        </a>
                            </div>
                            <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('customer.index') }}" class="hvr-pop">
                                        <i class="fas fa-user-tie" style="font-size:30px;"></i> 
                                        <p class="label_font">ข้อมูลลูกค้า </p>     
                                        </a>
                            </div>
                            <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('van.index') }}" class="hvr-pop">
                                        <i class="fas fa-shipping-fast" style="font-size:30px;"></i> 
                                        <p class="label_font">ข้อมูลรถตู้ </p>     
                                        </a>
                            </div>
                            <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('driver.index') }}" class="hvr-pop">
                                        <i class="far fa-address-card" style="font-size:30px;"></i> 
                                        <p class="label_font">ข้อมูลคนขับ </p>     
                                        </a>
                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('reservation.status', 0) }}" class="hvr-pop">
                                    <i class="far fa-calendar-check" style="font-size:30px;"></i> 
                                    <p class="label_font"> ข้อมูลการจอง </p>     
                                </a>
                            </div>
                             <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('reservation.status', 1) }}" class="hvr-pop">
                                    <i class="far fa-money-bill-alt" style="font-size:30px;"></i> 
                                    <p class="label_font"> ข้อมูลแจ้งชำระเงิน </p>     
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('reservation.status', 3) }}" class="hvr-pop">
                                    <i class="far fa-times-circle" style="font-size:30px;"></i> 
                                    <p class="label_font"> ไม่อนุมัติ </p>     
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('reservation.status', 2) }}" class="hvr-pop">
                                    <i class="fa fa-dollar-sign" style="font-size:30px;"></i> 
                                    <p class="label_font"> อนุมัติชำระเงิน </p>     
                                </a>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- column -->
        </div>
        <br>
        <div class="row bg-white m-l-0 m-r-0 box-shadow ">
            <!-- column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">SETTING</h4>
                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('user.index') }}" class="hvr-pop">
                                        <i class="fa fa-users" style="font-size:30px;"></i> 
                                        <p class="label_font"> ผู้ใช้งานระบบ </p>     
                                        </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- column -->
        </div>
        @else
        <div class="row bg-white m-l-0 m-r-0 box-shadow ">
            <!-- column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">MENU</h4>
                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('home') }}" class="hvr-pop">
                                    <i class="fa fa-tachometer" style="font-size:30px;"></i> 
                                    <p class="label_font"> หน้าหลัก </p>     
                                </a>
                            </div>
                             <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('reservation.create') }}" class="hvr-pop">
                                    <i class="fas fa-shipping-fast" style="font-size:30px;"></i> 
                                    <p class="label_font"> จองรถตู้ </p>     
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('reservation.status', 0) }}" class="hvr-pop">
                                    <i class="far fa-address-card" style="font-size:30px;"></i> 
                                    <p class="label_font"> ข้อมูลการจอง </p>     
                                </a>
                            </div>
                             <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('reservation.status', 1) }}" class="hvr-pop">
                                    <i class="far fa-money-bill-alt" style="font-size:30px;"></i> 
                                    <p class="label_font"> ข้อมูลแจ้งชำระเงิน </p>     
                                </a>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('reservation.status', 3) }}" class="hvr-pop">
                                    <i class="far fa-times-circle" style="font-size:30px;"></i> 
                                    <p class="label_font"> ไม่อนุมัติ </p>     
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3 animated fadeInRight text-center">
                                <a href="{{ route('reservation.status', 2) }}" class="hvr-pop">
                                    <i class="fa fa-dollar-sign" style="font-size:30px;"></i> 
                                    <p class="label_font"> อนุมัติชำระเงิน </p>     
                                </a>
                            </div>
                             
                        </div>
                    </div>
                </div>
            </div>
            <!-- column -->
        </div>
        <br>
        <!-- End PAge Content -->
        @endif

    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->
@endsection