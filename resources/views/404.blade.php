@extends('layouts.app') 
@section('title', '404 Page') 
@section('content')
     <!-- Main wrapper  -->
    <div class="error-page" id="wrapper">
        <div class="error-box">
            <div class="error-body text-center">
                <h1>404</h1>
                <h3 class="text-uppercase">Page not found </h3>
                <p class="text-muted m-t-30 m-b-30 font" style="font-size:50px">คุณไม่สิทธิ์ในการใช้งานหน้านี้</p>
                <button onclick="goBack()" class="btn btn-warning btn-lg"><span class="ion ion-arrow-left-a"></span> Go Back </button> </div>
        </div>
    </div>
    <!-- End Wrapper -->
@endsection 
@push('scripts')
<script>
  function goBack() { window.history.back() }
</script>
@endpush