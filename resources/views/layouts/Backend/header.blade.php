        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ route('home') }}">
                        <!-- Logo icon -->
                        <b><span class="dark-logo"></span>  TRP </b>
                        <!--End Logo icon -->
                        <span class="dark-logo">Tour</span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav ml-auto mt-md-0">
                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Html::image('images/backend/users/'.Auth::user()->image, "User",['class'=>'profile-pic']) }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    @if(Auth::user()->hasRole('Admin'))
                                    <li><a href="{{ route('user.show', Auth::user()->id) }}"><i class="ti-user"></i> Profile</a></li>
                                    @endif
                                    @if(Auth::user()->hasRole('User'))
                                    <li><a href="{{ route('customer.show', Auth::user()->customer_id) }}"><i class="ti-user"></i> Profile</a></li>
                                    @endif
                                    <li>
                                        <a href="#" onclick="event.preventDefault();document.getElementById('logout-form-header').submit();"><i class="fa fa-power-off"></i> Logout</a>
                                        {!! Form::open(['route' => 'logout', 'id'=>'logout-form-header']) !!} {!! Form::close() !!}
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->