@php
use Libraries\NotificationLibrary\NotificationLibrary;
@endphp
<!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">MENU</li>
                        <li> 
                            <a href="{{ route('home') }}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu"> หน้าหลัก </span></a>
                        </li>
                        @if(Auth::user()->hasRole('Admin'))
                        <li> 
                            <a href="{{ route('customer.index') }}" aria-expanded="false"><i class="fas fa-user-tie"></i><span class="hide-menu"> ข้อมูลลูกค้า </span></a>
                        </li>
                         <li> 
                            <a href="{{ route('driver.index') }}" aria-expanded="false"><i class="far fa-address-card"></i><span class="hide-menu"> ข้อมูลคนขับ </span></a>
                        </li>

                        <li> 
                            <a href="{{ route('van.index') }}" aria-expanded="false"><i class="fas fa-shipping-fast"></i><span class="hide-menu"> ข้อมูลรถตู้ </span></a>
                        </li>
                       
                        <li> 
                            <a href="{{ route('reservation.status', 0) }}" aria-expanded="false"><i class="far fa-calendar-check"></i><span class="hide-menu"> ข้อมูลการจอง </span></a>
                        </li>
                        <li> 
                            <a href="{{ route('reservation.status', 1) }}" aria-expanded="false"><i class="far fa-money-bill-alt"></i><span class="hide-menu"> ข้อมูลแจ้งชำระเงิน </span></a>
                            @if(NotificationLibrary::read())
                            <span class="label label-rouded label-primary pull-right hide-menu" style="margin-top:-31px;margin-right:10px">{{ NotificationLibrary::read() }}</span>
                            @endif
                        </li>
                        <li> 
                            <a href="{{ route('reservation.status', 3) }}" aria-expanded="false"><i class="far fa-times-circle"></i><span class="hide-menu"> ไม่อนุมัติ </span></a>
                        </li>
                         <li> 
                            <a href="{{ route('reservation.status', 2) }}" aria-expanded="false"><i class="fas fa-dollar-sign"></i><span class="hide-menu"> อนุมัติชำระเงิน </span></a>
                        </li>
                        <li> 
                            <a href="{{ route('post.index') }}" aria-expanded="false"><i class="far fa-list-alt"></i><span class="hide-menu"> บทความ </span></a>
                        </li>
                        @else

                        <li> 
                            <a href="{{ route('reservation.create') }}" aria-expanded="false"><i class="fas fa-shipping-fast"></i><span class="hide-menu"> จองรถตู้ </span></a>
                        </li>
                        <li> 
                            <a href="{{ route('reservation.status', 0) }}" aria-expanded="false"><i class="far fa-calendar-check"></i><span class="hide-menu"> ข้อมูลการจอง </span></a>
                        </li>
                        <li> 
                            <a href="{{ route('reservation.status', 1) }}" aria-expanded="false"><i class="far fa-money-bill-alt"></i><span class="hide-menu"> ข้อมูลแจ้งชำระเงิน </span></a>
                        </li>
                         <li> 
                            <a href="{{ route('reservation.status', 3) }}" aria-expanded="false"><i class="far fa-times-circle"></i><span class="hide-menu"> ไม่อนุมัติ </span></a>
                        </li>
                         <li> 
                            <a href="{{ route('reservation.status', 2) }}" aria-expanded="false"><i class="fas fa-dollar-sign"></i><span class="hide-menu"> อนุมัติชำระเงิน </span></a>
                        </li>
                       
                        @endif

                        @if(Auth::user()->hasRole('Admin'))
                        <li class="nav-label">SETTING</li>
                        <li> 
                            <a href="{{ route('user.index') }}" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu"> ผู้ใช้งานระบบ </span></a>
                        </li>
                        @endif
                        <li class="nav-label">USER</li>
                        @if(Auth::user()->hasRole('Admin'))
                        <li><a href="{{ route('user.show', Auth::user()->id) }}"><i class="ti-user"></i> Profile</a></li>
                        @endif
                        @if(Auth::user()->hasRole('User'))
                        <li><a href="{{ route('customer.show', Auth::user()->customer_id) }}"><i class="ti-user"></i> Profile</a></li>
                        @endif
                        <li>
                            <a href="#" onclick="event.preventDefault();document.getElementById('logout-form-menu').submit();"><i class="fa fa-power-off"></i> Logout</a>
                            {!! Form::open(['route' => 'logout', 'id'=>'logout-form-menu']) !!} {!! Form::close() !!}
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->