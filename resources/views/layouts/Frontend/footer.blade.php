<div id="footer">
  <div class="container text-center">
    <div class="fnav">
      <p style="font-size:20px;">Copyright &copy; {{ Carbon::now()->year }} By <a href="{{ route('/') }}">Teerapong Tour</a></p>
    </div>
  </div>
</div>