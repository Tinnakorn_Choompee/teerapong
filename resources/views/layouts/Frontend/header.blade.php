<!-- Header -->
<header id="header">
  <div class="intro text-center">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="intro-text">
            <h1>Welcome to <span class="brand">Teerapong Tour</span></h1>
            <p style="font-size:30px">บริษัท ธีรพงษ์ ทัวร์จำกัด มุ่งมั่น พัฒนา เอาใจใส่ เพื่อจะเป็นผู้ให้บริการรถตู้ VIP ที่ดีที่สุดในประเทศไทย</p>
            <a href="{{ route('register') }}" class="btn btn-default btn-lg page-scroll font" style="color:#fff;font-size:25px;margin-right:20px">ลงทะเบียน</a> 
            <a href="{{ route('login') }}"    class="btn btn-default btn-lg page-scroll font" style="color:#fff;font-size:25px">เข้าสู่ระบบ</a> 
          </div>
        </div>
      </div>
    </div>
  </div>  
</header>