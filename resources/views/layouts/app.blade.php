<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" href="{{ asset('images/frontend/Van.png') }}" type="image/x-icon" />
    <title>@yield('title')</title>
    <!-- Bootstrap Core CSS -->
    {{ Html::style('css/Backend/lib/bootstrap/bootstrap.min.css') }}
    <!-- Font-awesome -->
    {{ Html::style('fonts/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css') }}
    <!-- Hover -->
    {{ Html::style('css/Backend/lib/hover/hover-min.css') }}
    <!-- Custom CSS -->
    {{ Html::style('css/Backend/helper.css') }}
    {{ Html::style('css/Backend/style.css') }}
    {{ Html::style('css/Backend/hover-min.css') }}
    @yield('styles')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar">
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        @auth
            @include('layouts.Backend.header')
            @include('layouts.Backend.sidebar')
            @yield('content')
            @include('layouts.Backend.footer')
        @endauth
            @yield('login')
    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    {{ Html::script('js/backend/lib/jquery/jquery-3.3.1.min.js') }}
    <!-- Bootstrap tether Core JavaScript -->
    {{ Html::script('js/backend/lib/bootstrap/js/popper.min.js') }}
    {{ Html::script('js/backend/lib/bootstrap/js/bootstrap.min.js') }}
    <!-- slimscrollbar scrollbar JavaScript -->
    {{ Html::script('js/backend/jquery.slimscroll.js') }}
    <!--Menu sidebar -->
    {{-- {{ Html::script('js/backend/sidebarmenu.js') }} --}}
    <!--stickey kit -->
    {{ Html::script('js/backend/lib/sticky-kit-master/dist/sticky-kit.min.js') }}
    <!--Sweetalert -->
    {{ Html::script('js/backend/lib/sweetalert/sweetalert.min.js') }}
    <!--Custom JavaScript -->
    {{ Html::script('js/backend/custom.min.js') }}
    @stack('scripts')
</body>

</html>