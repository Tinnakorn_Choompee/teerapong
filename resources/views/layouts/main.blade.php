<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ธีรพงษ์ทัวน์ | Teerapong Tour</title>
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="icon" href="{{ asset('images/frontend/Van.png') }}" type="image/x-icon" />
<!-- Bootstrap -->
{{ Html::style('css/frontend/bootstrap.css') }}
{{ Html::style('fonts/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css') }}

<!-- Stylesheet
    ================================================== -->
{{ Html::style('css/frontend/style.css') }}
{{ Html::style('css/frontend/prettyPhoto.css') }}
{{ Html::style('http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300') }}

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        @include('layouts.Frontend.nav')
        @include('layouts.Frontend.header')
        @yield('content')
        @include('layouts.Frontend.footer')
        {{ Html::script('js/frontend/jquery.1.11.1.js') }}
        {{ Html::script('js/frontend/smooth-scroll.js') }}
        {{ Html::script('js/frontend/bootstrap.js') }}
        {{ Html::script('js/frontend/jquery.prettyPhoto.js') }}
        {{ Html::script('js/frontend/jquery.isotope.js') }}
        {{ Html::script('js/frontend/jqBootstrapValidation.js') }}
        {{ Html::script('js/frontend/main.js') }}
    </body>
</html>