@extends('layouts.app') 
@section('title', 'ข้อมูลรถตู้')
@section('styles')
{{ Html::style('plugin/fancybox-master/dist/jquery.fancybox.min.css') }}
{{ Html::style('plugin/fullcalendar-3.9.0/fullcalendar.min.css') }}
{{ Html::style('plugin/fullcalendar-3.9.0/fullcalendar.print.min.css', ['media'=>'print']) }}
<style>
     #calendar {
        margin: 0 auto;
    }
    .fc-title {
        color: #fff;
        text-align: center;
        font-size: 20px;
        margin: 0 auto;
    }
    .fc-content {
        text-align: center;
        margin: 0 auto;
    }
    .fc-time {
        display:none;
    }

</style>
@endsection 
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary title-header">ข้อมูลรถตู้</h3>
        </div>
    @include('layouts.Backend.breadcrumb')
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div id="invoice" class="effect2">
                    <div id="invoice-mid">
                        <div class="row">
                            <div id="calendar"></div>
                        </div>
                    </div>
                    <!--End Invoice Mid-->
                </div>
                <!--End Invoice-->
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->
@endsection
@push('scripts')
{{ Html::script('plugin/fullcalendar-3.9.0/lib/moment.min.js') }}
{{-- {{ Html::script('plugin/fullcalendar-3.9.0/lib/jquery.min.js') }} --}}
{{ Html::script('plugin/fullcalendar-3.9.0/fullcalendar.min.js') }}
{{ Html::script('plugin/fullcalendar-3.9.0/locale/th.js') }}
<script>
$(document).ready(function() {
    $("#calendar").fullCalendar({
    header: {
        left: 'prev,next',
        center: 'title',
        right: 'today'
    },
    defaultDate: new Date(),
    navLinks: true,
    editable: false,
    eventLimit: true,
    events: [
        @foreach($van->reservation as $reservation)
            @if($reservation->status == 2)
            {
            title: 'จอง',
            start: "{{ $reservation->start_date }}",
            end: "{{ Carbon::parse($reservation->end_date)->addDay(1) }}"
            },
        @endif
        @endforeach
    ]
    });
    
});
</script>
@endpush