@extends('layouts.app') 
@section('title', 'แก้ไขข้อมูลคนขับ') 
@section('content')
       <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary title-header">แก้ไขข้อมูลรถตู้</h3> 
                </div>
                @include('layouts.Backend.breadcrumb')
            </div>
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-primary">
                            <div class="card-body">
                                    {!! Form::model($van, ['route' => ['van.update', $van->id], 'files'=>TRUE, 'method'=>'PATCH']) !!}
                                    {!! Form::hidden('id', $van->id) !!}
                                    {!! Form::hidden('edit_image', $van->image) !!}
                                    <div class="form-body">
                                        <h3 class="card-title m-t-15 font">ข้อมูลรถตู้</h3>
                                        <hr>
                                        <div class="row p-t-20">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('number', 'เลขทะเบียนประจำรถตู้', ['class'=>'label_font']) !!}
                                                    {!! Form::text('number', NULL, ['class'=>'form-control', 'required']) !!}
                                                    @if ($errors->has('number'))
                                                        <small class="form-control-feedback text-danger"> {{ $errors->first('number') }} </small> 
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('brand', 'ยี่ห้อ', ['class'=>'label_font']) !!}
                                                    {!! Form::text('brand', NULL, ['class'=>'form-control', 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('model', 'รุ่น', ['class'=>'label_font']) !!}
                                                    {!! Form::text('model', NULL, ['class'=>'form-control', 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('rate', 'ราคาเช่า/วัน', ['class'=>'label_font']) !!}
                                                    {!! Form::number('rate', NULL, ['class'=>'form-control', 'required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->  
                                        <div class="row">     
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('driver', 'รายชื่อคนขับ', ['class'=>'label_font']) !!}
                                                    @if($van->driver_id == 0)
                                                    {!! Form::select('driver', $driver , $van->driver_id ,['class'=>'form-control', 'required', 'placeholder'=> 'เลือกคนขับ']) !!}
                                                    @else 
                                                    {!! Form::hidden('driver', $van->driver_id) !!}
                                                    <p class="font" style="font-size:20px;margin-left:0px">{{ $van->driver->prename }} {{ $van->driver->name." ".$van->driver->surname }}</p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('image', 'รูปภาพ', ['class'=>'label_font']) !!}
                                                    {!! Form::file('image', ['class'=>'form-control', 'id' => 'img']); !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3 text-center">
                                                <div class="form-group">
                                                    {{ Html::image('images/backend/vans/'.$van->image , "User",['width'=>'120','height'=>'120', 'id'=>'blah']) }}
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success label_font"> <i class="fa fa-check"></i> บันทึก</button>
                                        <a href="{{ route('van.index') }}" class="btn btn-inverse label_font">ยกเลิก</a>
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row -->
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
        </div>
        <!-- End Page wrapper  -->
@endsection 
@push('scripts')
<script>


    function readImage(input) {

    if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
    }
    }

    $("#img").change(function() {
        readImage(this);
    });

</script>
@endpush