@extends('layouts.app') 
@section('title', 'ข้อมูลรถตู้') 
@section('styles')
{{ Html::style('plugin/fancybox-master/jquery.fancybox.min.css') }}
{{ Html::style('plugin/fullcalendar-3.9.0/fullcalendar.min.css') }}
{{ Html::style('plugin/fullcalendar-3.9.0/fullcalendar.print.min.css', ['media'=>'print']) }}
<style>
     #calendar {
        margin: 0 auto;
    }
    .fc-title {
        color: #fff;
        text-align: center;
        font-size: 20px;
        margin: 0 auto;
    }
    .fc-content {
        text-align: center;
        margin: 0 auto;
    }
    .fc-time {
        display:none;
    }

</style>
@endsection
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary title-header">ข้อมูลรถตู้</h3>
        </div>
    @include('layouts.Backend.breadcrumb')
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div id="invoice" class="effect2">
                    <div id="invoice-mid">
                        <div class="row">
                            <div class="col-md-2 text-center">
                                {{ Html::image('images/backend/vans/'.$van->image, "Van",['width'=>'100','height'=>'100', 'class'=>'img-responsive img-circle']) }}                         
                            </div>
                            <div class="col-md-10">
                                <div class="invoice-info">
                                    <h2 class="label_font">เลขทะเบียนประจำรถตู้</h2>
                                    <p class="label_font">{{ $van->number }}</p>
                                    <h2 class="label_font">รายชื่อคนขับ</h2>
                                    @if($van->driver_id == 0)
                                    <span class="label label-rouded label-danger label_font" style="font-size:20px">ไม่มีคนขับ</span> 
                                    @else
                                    <p class="label_font">{{ $van->driver->prename == 1 ? "นาย " : "นาง " }}{{ $van->driver->name." ".$van->driver->surname}}<br>                            
                                    @endif
                                    <h2 class="label_font">ราคา / วัน</h2>
                                    <p class="label_font">{{ $van->rate }}</p>
                                </div>
                                <div id="project">
                                    <h2 class="label_font">ยี่ห้อ</h2>
                                    <p class="label_font">{{ $van->brand }}</p>
                                    <h2 class="label_font">รุ่น</h2>
                                    <p class="label_font">{{ $van->model }}</p>
                                    <h2 class="label_font"> ตารางการจอง </h2>
                                    <p class="label_font" style="font-size:30px !important;"> <a data-fancybox data-src="#selectableModal" href="javascript:;" class="btn btn-info"> ปฏิทินการจอง </a></p>
                                </div>
                            </div>
                        </div>
                        <div style="display: none;max-width:800px;" id="selectableModal">
                            <div data-selectable="true" id="calendar"></div>
                        </div>
                    </div>
                    <!--End Invoice Mid-->
                </div>
                <!--End Invoice-->
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->
@endsection
@push('scripts')
{{ Html::script('plugin/fullcalendar-3.9.0/lib/moment.min.js') }}
{{ Html::script('plugin/fullcalendar-3.9.0/fullcalendar.min.js') }}
{{ Html::script('plugin/fullcalendar-3.9.0/locale/th.js') }}
{{ Html::script('plugin/fancybox-master/jquery.fancybox.min.js') }}
<script>
$(document).ready(function() {
    $("#calendar").fullCalendar({
    header: {
        left: 'prev,next',
        center: 'title',
        right: 'today'
    },
    defaultDate: new Date(),
    navLinks: true,
    editable: false,
    eventLimit: true,
    events: [
        @foreach($van->reservation as $reservation)
            @if($reservation->status == 2)
            {
            title: 'จอง',
            start: "{{ $reservation->start_date }}",
            end: "{{ Carbon::parse($reservation->end_date)->addDay(1) }}"
            },
        @endif
        @endforeach
    ]
    });
    
});
</script>
@endpush