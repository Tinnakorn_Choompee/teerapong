@php
use Libraries\DateThaiLibrary\DateThaiLibrary;
@endphp
@extends('layouts.app') 
@section('title', 'ข้อมูลการจองรถตู้') 
@section('styles')
{{ Html::style('css/Backend/flatpickr.min.css') }}
{{ Html::style('plugin/fancybox-master/jquery.fancybox.min.css') }}
<style>
.form-control:disabled, .form-control[readonly] {
    background-color: white;
}
</style>
@endsection
@section('content')
       <!-- Page wrapper  -->
       <div class="page-wrapper">
        <!-- Bread crumb -->
        <div class="row page-titles">
          <div class="col-md-5 align-self-center">
              <h3 class="text-primary title-header">ข้อมูลการจองรถตู้</h3> 
          </div>
            <div class="col-md-7 align-self-center font">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">หน้าหลัก</a></li>
                    <li class="breadcrumb-item"><a style="cursor:pointer" onclick="goBack()">ข้อมูลการจอง</a></li>
                    <li class="breadcrumb-item active">ดูข้อมูลการจอง</li>
                </ol>
            </div>
        </div>
      <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-lg-12">
                    <div id="invoice" class="effect2">
                            <hr>
                            <div class="row">
                                <div class="col-md-5">
                                        <h3 class="font"> สามารถชำระเงินได้โดยการโอนเงินผ่านธนาคารเข้ามายังบัญชีต่อไปนี้</h3>
                                </div>
                                <div class="col-md-2 text-center">
                                        <strong class="text-primary"> 847-2-30811-5 </strong> 
                                </div>
                                <div class="col-md-5">
                                        <span class="pull-right text-waring" style="font-size:21px;">  กสิกรไทย สาขา เซ็นทรัล  ชื่อบัญชี TeerapongTour </span> 
                                </div>
                            </div>
                            <hr>
                        <div id="invoice-mid">
                          <div class="row justify-content-md-center">
                            <div class="col-md-3 text-center">
                                {{ Html::image('images/backend/vans/'.$reservation->van->image, NULL, ['class'=>'img-responsive', 'style'=>'width:80% !important']) }}
                                <br><br>
                                @if(Auth::user()->hasRole('User'))
                                    @if($reservation->status == 0)
                                    <button type="button" class="btn btn-primary btn-edit font" data-price="{{ $reservation->price }}" style="font-size:22px" data-toggle="modal" data-target="#reservation"> แจ้งชำระเงิน </button>
                                    @elseif($reservation->status == 1)
                                    <button type="button" class="btn btn-warning btn-edit font disabled" style="font-size:22px; color:black"> รอการอนุมัติการชำระเงิน </button>
                                    @elseif($reservation->status == 2)
                                    <button type="button" class="btn btn-success btn-edit font disabled" style="font-size:22px; color:black"> อนุมัติชำระเงินเรียบร้อย </button>
                                    @elseif($reservation->status == 3)
                                    <button type="button" class="btn btn-danger btn-edit font disabled" style="font-size:22px; color:black"> ไม่อนุมัติการชำระเงิน </button>
                                    <br>
                                    <p class="text-danger font"> สาเหตุ  {{ $reservation->note }}</p>
                                    @endif
                                @endif
                            </div>
                            <div class="col-md-9">
             
                                <div class="col"> เลขทะเบียน  <span class="float-right">{{ $reservation->van->number }}</span></div>

                                <div class="col"> ยี่ห้อ <span class="float-right">{{ $reservation->van->brand }}</span> </div>
                            
                                <div class="col"> รุ่น  <span class="float-right">{{ $reservation->van->model }}</span></div>
                          
                                <div class="col"> คนขับ <span class="float-right">{{ $reservation->driver->prename == 1 ? "นาย " : "นาง " }}{{ $reservation->driver->name." ".$reservation->driver->surname}}</span></div>
                                       
                                <hr>
                        
                                <div class="col"> ค่าเช่า <span class="float-right">{{ $reservation->van->rate." บ. / วัน" }}</span> </div>

                                <hr>
                             
                                <div class="col text-center"> 
                                    @if($reservation->type == 1)
                                            <label class="control-label">ข้อมูลการจอง <span class="text-danger">>></span> วันที่ {{ Carbon::parse($reservation->start_date)->format('d/m/Y')." ถึง ".Carbon::parse($reservation->end_date)->format('d/m/Y') }}  <span class="text-danger"><<</span>   
                                                <span class="text-primary"> (จำนวน {{ $reservation->numdate}} วัน รวมเงิน {{ $reservation->price }} บาท) </span>  
                                            </label>
                                    @else
                                            <label class="control-label">ข้อมูลการจอง <span class="text-danger">>></span> วันที่ {{ $reservation->start_date." ถึง ".$reservation->end_date }}  <span class="text-danger">>></span>
                                                <span class="text-primary"> (จำนวน {{ $reservation->numdate}} วัน รวมเงิน {{ $reservation->price }} บาท) </span>  
                                            </label>
                                    @endif
                                </div>
                                <hr>
                                <div class="col"> ข้อมูลลูกค้า  <span class="float-right">{{ $reservation->customer->name }}</span></div>

                                <div class="col"> เบอร์โทรลูกค้า  <span class="float-right">{{ $reservation->customer->telephone }}</span></div>
                            </div>
                            </div>
                          </div>

                          @if(in_array($reservation->status, [1,2,3]))
                            @if(Auth::user()->hasRole('Admin'))
                            <hr>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                        <h3 class="font"> หลักฐานการชำระเงิน </h3>
                                </div>
                            </div>
                            <hr>
                            <div class="row justify-content-md-center">
                                    <div class="col-md-3 text-center">
                                            <a data-fancybox="gallery" href="{{asset('images/backend/payment/'.$reservation->payment->image)}}">
                                                {{ Html::image('images/backend/payment/'.$reservation->payment->image, NULL, ['class'=>'', 'width'=>'280','height'=>'250']) }}
                                            </a>
                                        <br><br>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="col"> วันที่โอน  <span class="float-right">{{ Carbon::parse($reservation->payment->date)->format('d/m/Y') }}</span></div>
                                        <div class="col"> เวลาที่โอน <span class="float-right">{{ $reservation->payment->time }}</span> </div>
                                        <hr>
                                        <div class="col"> ยอดเงิน <span class="float-right"> {{ $reservation->payment->total }} </span> </div>
                                        <hr>
                                        <br>
                                        @if(in_array($reservation->status, [1]))
                                        <button class="btn btn-success btn-approve btn-block m-b-10 text-center font" style="font-size:22px;"> อนุมัติ </button>    
                                        <button class="btn btn-danger  btn-not btn-block m-b-10 text-center font" style="font-size:22px;"> ไม่อนุมัติ </button> 
                                        @elseif(in_array($reservation->status, [2])) 
                                        <div class="text-success"> อนุมัติชำระเงินเรียบร้อย </div>
                                        @elseif(in_array($reservation->status, [3])) 
                                        <div class="text-danger"> ไม่อนุมัติชำระเงิน  <span class="float-right"> สาเหตุ  {{ $reservation->note }} </span></div>
                                        @endif
                                    </div>
                                    </div>
                                </div>
                              @endif
                          @endif

                        </div>
                        <!--End Invoice Mid-->
                    </div>
                    <!--End Invoice-->
                </div>
            </div>
            <!-- End PAge Content -->
        </div>
        <!-- End Container fluid  -->
    </div>
    <!-- End Page wrapper  -->

{{ Form::open(['route'=>['reservation.apporve', $reservation->id], 'id'=>'apporve-form']) }}
{{ Form::hidden('apporve', NULL, ['id'=>'apporve']) }}
{{ Form::hidden('note', NULL, ['id'=>'note']) }}
{{ Form::close() }}

@include('payment.create')
</div>
@endsection 
@push('scripts')
{{ Html::script('js/backend/flatpickr.js') }}
{{ Html::script('js/backend/th.js') }}
{{ Html::script('plugin/fancybox-master/jquery.fancybox.min.js') }}
@if (session('payment'))
<script>
    swal("Success!", "ทำการ แจ้งชำระเงิน เรียบร้อยแล้ว รอการอนุมัติจากผู้ดูแลระบบ", "success");
</script>
@endif
<script>
    function goBack() {
        window.history.back();
    } 

    $('#reservation').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var price = button.data('price')

        $('#total').val(price);
        $(".date").flatpickr({
            locale: "th",
            minDate : "today",
            defaultDate: new Date(),
        });         
        $(".time").flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            time_24hr: true,
            defaultDate: new Date(),
        });
    });

    $('.btn-approve').on('click',function(e){
        e.preventDefault();
        swal({
            title: "Are you sure?",
            text: "ต้องการที่อนุมัติ การชำระเงิน นี้ใช่หรือไม่ !!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
        .then(willDelete => {
            if(willDelete) {
                $('#apporve').val(1);
                $("#apporve-form").submit();  
            } else {
                return false;
            }
        });
    });

    $('.btn-not').on('click',function(e){
        e.preventDefault();
        swal("สาเหตุที่ไม่อนุมัติการชำระ", {
            content: "input",
        })
        .then((value) => {
            if(value) {
                $('#apporve').val(0);
                $('#note').val(value);
                $("#apporve-form").submit();  
            } else {
                return false;
            }
        });
    });
</script>
@endpush
   
   
