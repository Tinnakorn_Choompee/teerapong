@extends('layouts.app') 
@section('title', 'จองรถตู้ | Reservation')
@section('styles')
{{ Html::style('css/Backend/flatpickr.min.css') }}
{{ Html::style('plugin/fancybox-master/jquery.fancybox.min.css') }}
{{ Html::style('plugin/fullcalendar-3.9.0/fullcalendar.min.css') }}
{{ Html::style('plugin/fullcalendar-3.9.0/fullcalendar.print.min.css', ['media'=>'print']) }}
<style>
    .cursor-disbled {
        cursor: not-allowed !important;
    }
    .date {
        text-align:center !important;
        background-color:white !important;
        font-size:22px;
    }
</style>
@endsection 
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary title-header"> จองรถตู้ </h3>
        </div>
    @include('layouts.Backend.breadcrumb')
    </div>
    <!-- End Bread crumb -->
    
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="card-title font"> เลือกรถตู้ 
                                    @if( $errors->has('date')) 
                                    <span class="help-block text-danger" style="margin-left:20px;">
                                        <strong class="font" style="font-size:22px !important;"> >> {{ $errors->first('date') }} << </strong>
                                    </span>
                                    @endif
                                </h5>
                            </div>
                            {{-- <div class="col-md-6">
                                {!! Form::open(['route' => 'reservation.type']) !!}
                                <div class="form-group row" style="text-align: right;">
                                    <label class="control-label text-right col-md-9 font card-title"> สถานะ </label>
                                    <div class="col-md-3" id="custom-select">
                                        <select class="form-control" id="select_type" name="type">
                                            @if(!isset($type))
                                            <option value="" selected disabled>เลือก</option>
                                            <option value="" selected>ทั้งหมด</option>
                                            <option value="0">ว่าง</option>
                                            <option value="1">ไม่ว่าง</option>
                                            @elseif($type == 1)
                                            <option value="" selected disabled>เลือก</option>
                                            <option value="">ทั้งหมด</option>
                                            <option value="0">ว่าง</option>
                                            <option value="1" selected>ไม่ว่าง</option>
                                            @elseif($type == 0)
                                            <option value="" selected disabled>เลือก</option>
                                            <option value="" >ทั้งหมด</option>
                                            <option value="0" selected>ว่าง</option>
                                            <option value="1">ไม่ว่าง</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div> --}}
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        @foreach($vans as $van)
                        <div class="col-md-4">
                            <div class="card border">
                                <div class="row" style="margin:0 auto;">
                                    {{ Html::image('images/backend/vans/'.$van->image, NULL, ['class'=>'center', 'width'=>'220','height'=>'200']) }}
                                </div>
                     
                                <div class="card-body">
                                    <br> 
                                    {{-- @foreach($van->reservation as $reservation)
                                        @if($reservation->status == 2)
                                        {{ $van->id }}   
                                        {{ $reservation->start_date }}   ||     {{ $reservation->end_date }}
                                        <br>
                                        @endif
                                    @endforeach --}}
                                    <h5 class="card-title"> ดูตารางการจอง
                                    
                                    <span class="float-right"> 
                                    <a class="label label-rouded label-success" style="font-size:20px" href="{{ route('van.detail', $van->id) }}">
                                        ปฏิทิน
                                    </a>
                                    </span>
                                    
                                    </h5>

                                    <hr>
                                    <div class="row">
                                        <div class="col"> เลขทะเบียน </div>
                                        <div class="col"> <span class="float-right">{{ $van->number }}</span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col"> ยี่ห้อ </div>
                                        <div class="col"> <span class="float-right">{{ $van->brand }}</span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col"> รุ่น </div>
                                        <div class="col"> <span class="float-right">{{ $van->model }}</span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3"> คนขับ </div>
                                        <div class="col-md-9"> <span class="float-right">{{ $van->driver->prename == 1 ? "นาย " : "นาง " }}{{ $van->driver->name." ".$van->driver->surname}}</span></div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                    <div class="col-md-3">ค่าเช่า</div>
                                    <div class="col-md-9"> <span class="float-right">{{ $van->rate." บ. / วัน" }}</span></div>
                                    </div>
                                <hr>
                                    @if($van->status == 0)
                                    {{ Form::open(['route'=>'reservation.store', 'id'=>'form-'.$van->id]) }}
                                    {{ Form::hidden('van_id', $van->id) }}
                                    {{ Form::hidden('van_rate', $van->rate) }}
                                    {{ Form::hidden('driver_id', $van->driver->id) }}
                                    {{ Form::hidden('customer_id', Auth::user()->customer_id) }}
                                    <div class="row">
                                        <div class="col">
                                            <label>
                                                <input type="radio" class="option-input radio" name="type[{{$van->id}}]" value="1" 
                                                id="type_1_{{$van->id}}" checked  data-id="{{$van->id}}"/> <span class="font" style="font-size:22px;"> วันเดียว</span> 
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label>
                                                <input type="radio" class="option-input radio" name="type[{{$van->id}}]" value="2" 
                                                id="type_2_{{$van->id}}" data-id="{{$van->id}}"/> <span class="font" style="font-size:22px;">หลายวัน </span> 
                                            </label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="rate" value="{{$van->rate}}" id="rate_{{$van->id}}">
                                    <input type="text"   name="date" class="date form-control font" placeholder="เลือกวันที่จอง" required id="date_{{$van->id}}">
                                    <br>                            
                                    {{ Form::close() }}
                                    <button class="btn btn-primary btn-rounded btn-block m-b-10 text-center font btn-confrim hvr-pulse-grow" style="font-size:22px;" data-id="{{$van->id}}"> จอง </button>    
                                    @else
                                        <div class="row">
                                            <div class="col">
                                                <label>
                                                    <input type="radio" class="option-input radio cursor-disbled" name="{{$van->id}}" value="1" disabled/> <span class="font" style="font-size:22px;"> วันเดียว</span> 
                                                </label>
                                            </div>
                                            <div class="col">
                                                <label>
                                                    <input type="radio" class="option-input radio cursor-disbled" name="{{$van->id}}" value="2" disabled/> <span class="font" style="font-size:22px;">หลายวัน </span> 
                                                </label>
                                            </div>
                                        </div>
                                        <input type="text" name="date" class="date form-control font cursor-disbled" style=" text-align: center;background-color:#ccc;font-size:22px;"disabled>
                                        <br>                            
                                        <button class="btn btn-primary btn-rounded btn-block m-b-10 text-center font disabled cursor-disbled" style="font-size:22px;"> 
                                            ไม่ว่าง 
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:850px">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ข้อมูลการจอง</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

       <div id="calendar_1"></div>
       <div id="calendar_2"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection
@push('scripts')
    {{ Html::script('js/backend/flatpickr.js') }}
    {{ Html::script('js/backend/th.js') }}
    {{ Html::script('plugin/fancybox-master/jquery.fancybox.min.js') }}

    {{ Html::script('plugin/fullcalendar-3.9.0/lib/moment.min.js') }}
    {{-- {{ Html::script('plugin/fullcalendar-3.9.0/lib/jquery.min.js') }} --}}
    {{ Html::script('plugin/fullcalendar-3.9.0/fullcalendar.min.js') }}
    {{ Html::script('plugin/fullcalendar-3.9.0/locale/th.js') }}
    {{-- <script src='fullcalendar/locale/es.js'></script> --}}
    <!-- page script -->
    @if (session('success'))
    <script>
        swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @elseif (session('update'))
    <script>
        swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @elseif (session('delete'))
    <script>
        swal("Delete!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @endif


    @foreach($vans as $van)
        <script>
        var id   = "{{ $van->id }}";
        $("#date_"+id).flatpickr({
            locale: "th",
            minDate : "today",
            disable: [
                @foreach($van->reservation as $reservation)
                    @if($reservation->status == 2)
                    {
                        from: " {{ $reservation->start_date }}",
                        to: "{{ $reservation->end_date }}"
                    },                    
                    @endif
                @endforeach
            ]
        });
        </script>
    @endforeach

    @foreach($vans as $van)
    <script>
    var id  = "{{ $van->id }}";

    $("#type_1_"+id).change(function(){
        var van = $(this).data('id')
        $("#date_"+van).flatpickr({
            locale: "th",
            minDate : "today",
            disable: [
                    @foreach($van->reservation as $reservation)
                        @if($reservation->status == 2)
                        {
                            from: " {{ $reservation->start_date }}",
                            to: "{{ $reservation->end_date }}"
                        },                    
                        @endif
                    @endforeach
                ]
        });
    });

    $("#type_2_"+id).change(function(){
        var van = $(this).data('id')
        $("#date_"+van).flatpickr({
            locale: "th",
            mode: "range",
            dateFormat: "Y-m-d",
            minDate : "today",
            disable: [
                @foreach($van->reservation as $reservation)
                    @if($reservation->status == 2)
                        {
                            from: " {{ $reservation->start_date }}",
                            to: "{{ $reservation->end_date }}"
                        },                    
                    @endif
                @endforeach
            ]
        });
    });
    </script>
    @endforeach

    <script>
    $(".fancybox").fancybox();
    $('.btn-confrim').on('click', function(){
        var id   = $(this).data('id');
        var van  = $( ".radio:checked").val();
        var date = $( "#date_"+id).val();
        var rate = $( "#rate_"+id).val();
        if ($( "#date_"+id).val().length != 0){
            if(van == 1) {
            var days = 1;
            var rate = rate;
            var day_total = days;
        } else {
            var day = 2;
            var d   = date.split(' ถึง ');

            var startDay = new Date(d[1]);
            var endDay   = new Date(d[0]);
            var millisecondsPerDay = 1000 * 60 * 60 * 24;
            var millisBetween = startDay.getTime() - endDay.getTime();
            var days = millisBetween / millisecondsPerDay;
            var day_total = days + 1;
            var rate = (days + 1) * rate;
        }

        swal({
        title: "ยืนยันการจองรถตู้ ?",
        text: "วันที่ "+date+" เป็นเวลา "+day_total+" วัน รวม "+(rate)+" บาท",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["ยกเลิก", "ตกลง"],
        })
        .then((willConfrim) => {
        if (willConfrim) {
            $('#form-'+id).submit();
        } else {
           return false;
        }

        });

        } else {
           swal("กรุณาเลือกวันที่จอง");
        }
    });
   
    // $('#select_type').change(function() {
    //     this.form.submit();
    // });

    // $('#reservation').on('shown.bs.modal', function () {
        
    // })
    </script>


@endpush