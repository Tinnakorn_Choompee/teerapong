@php
use Libraries\DateThaiLibrary\DateThaiLibrary;
@endphp
@extends('layouts.app') 
@section('title', 'จองรถตู้ | Reservation')
@section('styles')
{{ Html::style('css/Backend/flatpickr.min.css') }}
<style>
    .cursor-disbled {
        cursor: not-allowed !important;
    }
</style>
@endsection 
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary title-header">จองรถตู้ </h3>
        </div>
        <div class="col-md-7 align-self-center font">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">หน้าหลัก</a></li>
                <li class="breadcrumb-item"><a style="cursor:pointer" onclick="goBack()">ข้อมูลการจอง</a></li>
                <li class="breadcrumb-item active">แก้ไขข้อมูลการจอง</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-primary">
                    <div class="card-body">
                      
                            <div class="form-body">
                                <h3 class="card-title m-t-15">แก้ไขข้อมูลการจอง</h3>
                                <hr>
                                <div class="row justify-content-md-center">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            {{ Html::image('images/backend/vans/'.$reservation->van->image, NULL, ['class'=>'card-img-top', 'width'=>'200','height'=>'200']) }}
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-5">
                                        <div class="form-group"> <br>    
                                            <div class="row">
                                                <div class="col"> เลขทะเบียน </div>
                                                <div class="col"> <span class="float-right">{{ $reservation->van->number }}</span></div>
                                            </div>
                                            <div class="row">
                                                <div class="col"> ยี่ห้อ </div>
                                                <div class="col"> <span class="float-right">{{ $reservation->van->brand }}</span></div>
                                            </div>
                                             <div class="row">
                                                <div class="col"> รุ่น </div>
                                                <div class="col"> <span class="float-right">{{ $reservation->van->model }}</span></div>
                                            </div>
                                            <div class="row">
                                                    <div class="col-md-3"> คนขับ </div>
                                                    <div class="col-md-9"> <span class="float-right">{{ $reservation->driver->prename == 1 ? "นาย " : "นาง " }}{{ $reservation->driver->name." ".$reservation->driver->surname}}</span></div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                <div class="col-md-3">ค่าเช่า</div>
                                                <div class="col-md-9"> <span class="float-right">{{ $reservation->van->rate." บ. / วัน" }}</span></div>
                                            </div>
                                            <hr>    
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                {{ Form::model($reservation ,['route'=>['reservation.update', $reservation->id], 'method'=>'PATCH', 'id'=>'form-'.$reservation->id]) }}
                                {{ Form::hidden('van_id', $reservation->van_id) }}
                                {{ Form::hidden('van_rate', $reservation->van->rate ) }}
                                {{ Form::hidden('driver_id', $reservation->driver_id) }}
                                {{ Form::hidden('customer_id', Auth::user()->customer_id) }}
                                <div class="row justify-content-md-center">
                                    <div class="col-md-3">
                                        <label class="control-label label_font">ประเภทวันที่จอง</label>
                                        <div class="row">
                                            <div class="col">
                                                <label>
                                                    <input type="radio" class="option-input radio" name="type[{{$reservation->id}}]" value="1" onclick="func({{$reservation->id}}, 1)" id="type_{{$reservation->id}}" {{ $reservation->type == 1 ? 'checked' : "" }}/>
                                                    <span class="font" style="font-size:22px;"> วันเดียว</span>
                                                </label>
                                            </div>
                                            <div class="col">
                                                <label>
                                                   <input type="radio" class="option-input radio" name="type[{{$reservation->id}}]" value="2" onclick="func({{$reservation->id}}, 2)" id="type_{{$reservation->id}}" {{ $reservation->type == 2 ? 'checked' : "" }}/>
                                                   <span class="font" style="font-size:22px;"> หลายวัน</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-5 text-center">
                                        <div class="form-group">
                                            @if($reservation->type == 1)
                                            <label class="control-label">ข้อมูลการจองเดิม <span class="text-danger">>></span> วันที่ {{ DateThaiLibrary::ThaiDate($reservation->start_date)." ถึง ".DateThaiLibrary::ThaiDate($reservation->end_date) }} <span class="text-danger"><<</span>   
                                                <span class="text-primary"> (จำนวน {{ $reservation->numdate}} วัน รวมเงิน {{ $reservation->price }} บาท) </span>  
                                            </label>
                                            @else
                                            <label class="control-label">ข้อมูลการจองเดิม <span class="text-danger">>></span> วันที่ {{ DateThaiLibrary::ThaiDate($reservation->start_date)." ถึง ".DateThaiLibrary::ThaiDate($reservation->end_date) }}  <span class="text-danger"><<</span> 
                                                <span class="text-primary"> (จำนวน {{ $reservation->numdate}} วัน รวมเงิน {{ $reservation->price }} บาท) </span>  
                                            </label>
                                            @endif
                                            <input type="hidden" name="rate" value="{{ $reservation->van->rate }}" id="rate_{{$reservation->id}}">
                                            <input type="text"   name="date" class="date form-control font" style=" text-align: center;background-color:white;font-size:22px;" placeholder="เลือกวันที่จอง" required id="date_{{$reservation->id}}">
                                            <br>                            
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                            <hr>
                            <div class="form-actions">
                                <button type="button" class="btn btn-success m-b-10 text-center label_font btn-confrim hvr-pulse-grow" style="font-size:22px;" data-id="{{ $reservation->id }}"> แก้ไขการจอง </button>  
                                <button type="button" class="btn btn-inverse m-b-10 label_font" onclick="goBack()"> ยกเลิก </button>
                            </div>
                            {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->
    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->

@endsection
@push('scripts')
    {{ Html::script('js/backend/flatpickr.js') }}
    {{ Html::script('js/backend/th.js') }}
    @if ($reservation->type == 1)
    <script>
        $(".date").flatpickr({
            locale: "th",
            minDate : "today",
        });
    </script>
    @else
    <script>
        $(".date").flatpickr({
            locale: "th",
            minDate : "today",
            mode: "range",
        });
    </script>
    @endif
    <!-- page script -->
    @if (session('success'))
    <script>
        swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @elseif (session('update'))
    <script>
        swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @elseif (session('delete'))
    <script>
        swal("Delete!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @endif 
    <script>
    function goBack() {
        window.history.back();
    } 
    function func(id, type) {
        $(".date").trigger('click');
        switch(type) {
            case 1:
            $(".date").flatpickr({
                locale: "th",
                minDate : "today",
                disable: [
                    {
                        from: "2018-06-19",
                        to: "2018-06-19"
                    },
                ]
            });
            break;

            case 2:
            $(".date").flatpickr({
                locale: "th",
                mode: "range",
                minDate : "today"
            });
            $(".date").trigger('click');
            break;
        }
    }

    $('.btn-confrim').on('click', function(){
        var id   = $(this).data('id');
        var van  = $( "#type_"+id+":checked" ).val();
        var date = $( "#date_"+id).val();
        var rate = $( "#rate_"+id).val();
        if ($( "#date_"+id).val().length != 0){
            if(van == 1) {
            var days = 1;
            var rate = rate;
            var day_total = days;
        } else {
            var day = 2;
            var d   = date.split(' ถึง ');

            var startDay = new Date(d[1]);
            var endDay   = new Date(d[0]);
            var millisecondsPerDay = 1000 * 60 * 60 * 24;
            var millisBetween = startDay.getTime() - endDay.getTime();
            var days = millisBetween / millisecondsPerDay;
            var day_total = days + 1;
            var rate = (days + 1) * rate;
        }

        swal({
        title: "ยืนยันการจองรถตู้ ?",
        text: "วันที่ "+date+" เป็นเวลา "+day_total+" วัน รวม "+(rate)+" บาท",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        buttons: ["ยกเลิก", "ตกลง"],
        })
        .then((willConfrim) => {
        if (willConfrim) {
            $('#form-'+id).submit();
        } else {
           return false;
        }

        });

        } else {
           swal("กรุณาเลือกวันที่จอง");
        }
    });
   

    $('#select_type').change(function() {
        this.form.submit();
    });

    $('#reservation').on('shown.bs.modal', function () {
        
    })
    </script>
@endpush