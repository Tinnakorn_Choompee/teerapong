@php
use Libraries\DateThaiLibrary\DateThaiLibrary;
@endphp
@extends('layouts.app') 
@section('title', 'ข้อมูลการจอง | Reservation') 
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary title-header">ข้อมูลการจอง</h3>
        </div>
    @include('layouts.Backend.breadcrumb')
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Admin Page Content -->
        <div class="row">
            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="far fa-address-card f-s-40 color-primary"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <a href="{{ route('reservation.status' , 0) }}">
                                <h2>{{ $reservation }}</h2>
                                <p class="m-b-0 label_font">ข้อมูลการจอง</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="far fa-money-bill-alt f-s-40 color-success"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <a href="{{ route('reservation.status' , 1) }}">
                                <h2>{{ $payment }}</h2>
                                <p class="m-b-0 label_font">แจ้งชำระเงิน</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="far fa-times-circle f-s-40 color-danger"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <a href="{{ route('reservation.status' , 3) }}">
                                <h2>{{ $not }}</h2>
                                <p class="m-b-0 label_font">ไม่อนุมัติ</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card p-30">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fas fa-dollar-sign f-s-40 color-warning"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <a href="{{ route('reservation.status' , 2) }}">
                                <h2>{{ $apporve }}</h2>
                                <p class="m-b-0 label_font">อนุมัติชำระเงิน</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <!-- End PAge Content -->
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        
                        @if(Auth::user()->hasRole('Admin'))
                        <h5 class="card-title font"> ตารางข้อมูลการจอง 
                            <span class="pull-right text-danger" style="font-size:21px;">** สามารถอนุมัติชำระเงินได้ที่ ปุ่ม <a href="#" data-toggle="tooltip" data-placement="bottom" class="btn btn-primary left" title="ดูข้อมูล"> <i class="fa fa-eye" aria-hidden="true"></i> </a> ดูหลักฐานการแจ้งชำระ</span>  
                    
                        @else
                        
                            @if($status == 0)
                            <h5 class="card-title font"> สามารถแจ้งชำระเงินได้ที่ ปุ่ม 
                                <button data-toggle="tooltip" data-placement="bottom" class="btn btn-primary left" title="ดูข้อมูล"> 
                                <i class="fa fa-eye" aria-hidden="true"></i> </button> ดูรายละเอียด การจอง 
                                <span class="pull-right text-danger" style="font-size:21px;">** กรณีไม่ได้แจ้งชำระเงินภายใน 3 ชั่วโมง ข้อมูลการจองของท่านจะถูกยกเลิกทันที</span> 
                            </h5>
                            @elseif($status == 1) 
                                <span class="pull-right text-warning" style="font-size:21px;"> รออนุมัติการชำระเงิน </span>  
                            @elseif($status == 2)
                                <span class="pull-right text-success" style="font-size:21px;"> อนุมัติชำระเงินเรียบร้อย </span>  
                            @elseif($status == 3)  
                                <span class="pull-right text-danger" style="font-size:21px;"> ไม่อนุมัติการชำระเงิน </span>  
                            @endif

                        @endif

                        </h5>

                        <div class="row">
                            <div class="col-md-5">
                                 
                            </div>
                        </div>
                        <hr>
                        <div class="table-responsive m-t-40">
                            <table id="example2" class="table table-bordered table-hover font">
                                <thead>
                                    <tr class="text-center">
                                        <th width="5%">#</th>
                                        <th width="10%">เลขทะเบียน</th>
                                        <th width="10%">วันที่จอง</th>
                                        <th width="10%">วันที่เริ่ม</th>
                                        <th width="10%">วันที่สิ้นสุด</th>
                                        <th width="7%">รวมวัน</th>
                                        <th width="10%">รวมเงิน</th>
                                        <th width="20%">สถานะการจอง</th>
                                        @if($status == 3)
                                        <th width="15%">สาเหตุ</th>
                                        @endif
                                        <th width="25%">ตัวเลือก</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr class="text-center">
                                        <th width="5%">#</th>
                                        <th width="10%">เลขทะเบียน</th>
                                        <th width="10%">วันที่จอง</th>
                                        <th width="10%">วันที่เริ่ม</th>
                                        <th width="10%">วันที่สิ้นสุด</th>
                                        <th width="7%">รวมวัน</th>
                                        <th width="10%">รวมเงิน</th>
                                        <th width="20%">สถานะการจอง</th>
                                        @if($status == 3)
                                        <th width="15%">สาเหตุ</th>
                                        @endif
                                        <th width="25%">ตัวเลือก</th>
                                    </tr>
                                </tfoot>
                                <tbody style="font-size:20px;" class="text-center">
                                    @foreach($reservations as $k => $reservation)
                                    <tr>
                                        <td class="text-center">{{ ++$k }}</td>
                                        <td class="text-center">{{ $reservation->van->number }}</td>
                                        <td class="text-center">{{ DateThaiLibrary::ThaiDate($reservation->reservation_date) }}</td>
                                        <td class="text-center">{{ DateThaiLibrary::ThaiDate($reservation->start_date) }}</td>
                                        <td class="text-center">{{ DateThaiLibrary::ThaiDate($reservation->end_date) }}</td>
                                        <td class="text-center">{{ $reservation->numdate }}</td>
                                        <td class="text-center">{{ $reservation->price }}</td>
                                        <td>
                                            @if($reservation->status == 0)
                                            <span class="label label-rouded label-danger" style="font-size:20px">ยังไม่ได้ชำระเงิน</span> 
                                            @elseif($reservation->status == 1)
                                            <span class="label label-rouded label-warning" style="font-size:20px">รออนุมัติการชำระเงิน</span>     
                                            @elseif($reservation->status == 2)
                                            <span class="label label-rouded label-success" style="font-size:20px">อนุมัติชำระเงินเรียบร้อย</span>        
                                            @elseif($reservation->status == 3)
                                            <span class="label label-rouded label-danger" style="font-size:20px">ไม่อนุมัติการชำระเงิน</span>        
                                            @endif
                                        </td>
                                        @if($status == 3)
                                        <td>
                                        {{ $reservation->note }} 
                                        </td>
                                        @endif
                                        <td class="text-center">
                                            <a href="{{ route('reservation.show', $reservation->id) }}" data-toggle="tooltip" data-placement="bottom" class="btn btn-primary left" title="ดูข้อมูล"> <i class="fa fa-eye" aria-hidden="true"></i> </a>
                                            @if(in_array($reservation->status, [1]))

                                            @elseif(in_array($reservation->status, [2,3]))
                                            @if(Auth::user()->hasRole('Admin'))
                                            <button class="btn btn-danger btn-del" data-id="{{ $reservation->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button>                                            
                                                {{ Form::open(['method' => 'DELETE', 'route' => ['reservation.destroy', $reservation->id], 'id'=>'form-delete-'.$reservation->id]) }} {{ Form::close() }}
                                            @endif
                                            @else
                                            <a class="btn btn-warning btn-edit" href="{{ route('reservation.edit', $reservation->id) }}" data-toggle="tooltip" data-placement="bottom"
                                                    title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i></a>
                                                <button class="btn btn-danger btn-del" data-id="{{ $reservation->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button>                                            
                                                {{ Form::open(['method' => 'DELETE', 'route' => ['reservation.destroy', $reservation->id], 'id'=>'form-delete-'.$reservation->id]) }} {{ Form::close() }}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
</div>
<!-- End Page wrapper  -->

@endsection
 @push('scripts')
<!-- DataTables -->
{{ Html::script('js/backend/lib/datatables/jquery.dataTables.js') }}
<!-- page script -->
@if (session('success'))
<script>
    swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('update'))
<script>
    swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('delete'))
<script>
    swal("Delete!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
</script>
@elseif (session('apporve'))
<script>
    swal("Apporved!", "ทำการอนุมัติการจองเรียบร้อยแล้ว", "success");
</script>
@elseif (session('not'))
<script>
    swal("Not allowed", "ไม่อนุมัติการจอง", "error");
</script>
@endif
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.btn-del').on('click',function(){
        var id = $(this).data('id');
        swal({
                title: "Are you sure?",
                text: "ต้องการที่จะลบ ข้อมูลการจอง นี้ใช่หรือไม่ !!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then(willDelete => {
                if (willDelete) { 
                    $( "#form-delete-"+id ).submit();
                }
            });
        });

        $('[data-toggle="tooltip"]').tooltip(); 

        $(function () {
                $('#report').DataTable({ "dom": 'rtp'});
                $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        });
</script>

@endpush